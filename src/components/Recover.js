import React from "react";
import {
  Text,
  SafeAreaView,
  View,
  TextInput,
  Button,
  StyleSheet,
  Alert
} from "react-native";
import {
  responsiveFontSize,
  responsiveHeight,
  responsiveWidth
} from "react-native-responsive-dimensions";
import * as firebase from "firebase";

export default class RecoverScreen extends React.Component {
  static navigationOptions = {
    title: "Recupera tu cuenta"
  };

  constructor(props) {
    super(props);
    this.state = {
      email: ""
    };
  }

  async _Recover() {
    try {
      await firebase
        .auth()
        .sendPasswordResetEmail(this.state.email)
        .then(function() {
          Alert.alert("Espera tu correo de recuperación");
        });
    } catch (e) {
      Alert.alert("Algo salió mal", e.toString());
    }
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.textContainerH2}>
          <Text style={styles.textLoginH1}>Escribe tu correo,</Text>
          <Text style={styles.textLoginH2}>para recuperar tu clave.</Text>
        </View>

        <TextInput
          style={styles.emailInput}
          placeholder={"Correo Electrónico"}
          placeholderTextColor={"#666666"}
          value={this.state.email}
          textContentType={"emailAddress"}
          blurOnSubmit={true}
          clearTextOnFocus={true}
          autoCapitalize={"none"}
          autoCorrect={false}
          onChangeText={text => this.setState({ email: text })}
        />
        <View
          style={{ width: responsiveWidth(80), marginTop: responsiveHeight(2) }}
        >
          <Button
            title="Acceder"
            color="#00A7E1"
            onPress={() => this._Recover()}
          />
        </View>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#f9f9f9"
  },
  textContainerH2: {
    width: responsiveWidth(80)
  },
  textLoginH1: {
    color: "#666666",
    fontSize: responsiveFontSize(4)
  },
  textLoginH2: {
    color: "#666666",
    fontSize: responsiveFontSize(2),
    textAlign: "left"
  },
  emailInput: {
    marginTop: responsiveHeight(4),
    borderColor: "#00A7E1",
    borderWidth: 2,
    width: responsiveWidth(80),
    height: responsiveHeight(6),
    borderRadius: 7,
    alignItems: "center",
    paddingLeft: responsiveWidth(5),
    backgroundColor: "#FFF"
  }
});
