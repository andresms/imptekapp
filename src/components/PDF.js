import React from "react";
import { View, SafeAreaView } from "react-native";
import PDFView from "react-native-view-pdf";
const resources = {
  url: "https://www.ets.org/Media/Tests/TOEFL/pdf/SampleQuestions.pdf"
};
export default class PDFScreen extends React.Component {
  static navigationOptions = {
    title: "Visualizar",
    headerStyle: {
      backgroundColor: "#20A5DC"
    },
    headerTitleStyle: {
      color: "white"
    }
  };
  render() {
    const { navigation } = this.props;
    const url = navigation.getParam("url", "NO INFO");

    return (
      <View style={{ flex: 1 }}>
        {/* Some Controls to change PDF resource */}
        <PDFView fadeInDuration={250.0} style={{ flex: 1 }} resource={url} />
      </View>
    );
  }
}
