import React from "react";
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  SafeAreaView,
  Animated
} from "react-native";
import {
  responsiveFontSize,
  responsiveHeight,
  responsiveWidth
} from "react-native-responsive-dimensions";
import CardView from "react-native-cardview";
import * as firebase from "firebase";

export default class PromoScreen extends React.Component {
  static navigationOptions = {
    title: "Promociones y ofertas",
    headerStyle: {
      backgroundColor: "#20A5DC"
    },
    headerTitleStyle: {
      color: "white"
    }
  };

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <SafeAreaView style={styles.fullContainer} bounces={false}>
        <Image
          style={styles.card1}
          source={require("../../src/images/promovip.png")}
        />
        <Image
          style={styles.card2}
          source={require("../../src/images/texto.png")}
        />
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  fullContainer: {
    flex: 1,
    backgroundColor: "#FFF",
    width: responsiveWidth(100),
    alignItems: "center",
    justifyContent: "center"
  },
  cardContainer1: {
    width: responsiveWidth(90),
    height: Math.round((responsiveWidth(90) * 1) / 2.42),
    marginLeft: "auto",
    marginRight: "auto",
    zIndex: 0,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 12
    },
    shadowOpacity: 0.58,
    shadowRadius: 16.0,
    elevation: 20,
    marginBottom: responsiveHeight(3)
  },
  cardContainerA: {
    width: responsiveWidth(90),
    height: Math.round((responsiveWidth(90) * 1) / 2.42),
    marginLeft: "auto",
    marginRight: "auto",
    zIndex: 0,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 12
    },
    shadowOpacity: 0.58,
    shadowRadius: 16.0,
    elevation: 20,
    marginBottom: responsiveHeight(3),
    marginTop: responsiveHeight(5)
  },
  card1: {
    resizeMode: "contain",
    width: responsiveWidth(89),
    height: Math.round((responsiveWidth(90) * 1) / 1.19)
  },
  card2: {
    resizeMode: "contain",
    width: responsiveWidth(89),
    height: Math.round((responsiveWidth(90) * 1) / 3.88)
  },
  cardContainerD: {
    width: responsiveWidth(90),
    height: Math.round((responsiveWidth(90) * 1) / 2.42),
    marginLeft: "auto",
    marginRight: "auto",
    zIndex: 0,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 12
    },
    shadowOpacity: 0.58,
    shadowRadius: 16.0,
    elevation: 20,
    marginBottom: responsiveHeight(3)
  },
  cardD: {
    resizeMode: "contain",
    width: responsiveWidth(90),
    height: Math.round((responsiveWidth(90) * 1) / 2.42)
  }
});
