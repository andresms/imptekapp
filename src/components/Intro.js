import React from "react";
import {
  View,
  Text,
  Image,
  Animated,
  StyleSheet,
  SafeAreaView,
  ImageBackground,
  TouchableOpacity,
  Linking,
  Alert,
  AsyncStorage
} from "react-native";
import {
  responsiveHeight,
  responsiveWidth,
  responsiveFontSize
} from "react-native-responsive-dimensions";
import SlidingPanel from "react-native-sliding-up-down-panels";
import CardView from "react-native-cardview";
import { YellowBox } from "react-native";

YellowBox.ignoreWarnings(["Warning"]);
const ACCESS_TOKEN = "access_token";
import * as firebase from "firebase";

export default class IntroScreen extends React.Component {
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);
    this.state = {
      animatedValue: new Animated.Value(0),
      isOn: false,
      logged: false
    };
  }

  componentDidMount() {
    let user = firebase.auth().currentUser;

    if (user) {
      this.setState({
        logged: true
      });
    } else {
      this.setState({
        logged: false
      });
    }
  }

  _animateIcon = () => {
    Animated.timing(this.state.animatedValue, {
      toValue: this.state.isOn ? 0 : 1,
      duration: 550,
      useNativeDriver: true
    }).start();
  };

  toggleHandle() {
    this.setState({ isOn: !this.state.isOn });
  }

  onLogout() {
    this.deleteToken();
  }

  async deleteToken() {
    try {
      await AsyncStorage.removeItem(ACCESS_TOKEN);
      this.props.navigation.navigate("Intro");
    } catch (error) {
      console.log("Something went wrong");
    }
  }

  render() {
    const interpolateRotation = this.state.animatedValue.interpolate({
      inputRange: [0, 1],
      outputRange: ["0deg", "-180deg"]
    });

    const animatedStyle = {
      transform: [{ rotate: interpolateRotation }]
    };
    if (this.state.logged) {
      return (
        <SafeAreaView style={styles.parentcontainer}>
          <Image
            style={styles.logo}
            source={require("../../src/images/logoimptek.png")}
            underlayColor="white"
          />
          <Text style={styles.text}>¿Qué deseas{"\n"}hacer hoy?</Text>
          <View style={styles.line} />
          <Text style={styles.textA}>actividades</Text>

          <CardView
            style={styles.cardContainer}
            cardElevation={7}
            cardMaxElevation={0}
            cornerRadius={5}
          >
            <View>
              <TouchableOpacity
                onPress={() => this.props.navigation.push("Loading")}
              >
                <Image
                  style={styles.card}
                  source={require("../../src/images/9convencion.png")}
                />
              </TouchableOpacity>
            </View>
          </CardView>
          <SlidingPanel
            panelPosition="top"
            allowDragging={false}
            headerLayoutHeight={responsiveHeight(40)}
            headerLayout={() => (
              <View style={styles.container}>
                <ImageBackground
                  style={styles.menuBg}
                  source={require("../../src/images/menuBg.png")}
                >
                  <Animated.Image
                    style={[styles.icon, animatedStyle]}
                    source={require("../../src/images/menuicon.png")}
                  />
                </ImageBackground>
              </View>
            )}
            onAnimationStart={this._animateIcon}
            onAnimationStop={() => this.toggleHandle()}
            slidingPanelLayoutHeight={responsiveHeight(100)}
            slidingPanelLayout={() => (
              <View style={styles.slidingPanelLayoutStyle}>
                <View style={styles.textContainer}>
                  <Text style={styles.nameMenu}></Text>
                  <View style={styles.linePanel} />
                  <View
                    style={{
                      flex: 1,
                      flexDirection: "row",
                      alignItems: "center",
                      height: responsiveHeight(10)
                    }}
                  >
                    <Image
                      style={styles.iconPanel}
                      source={require("../../src/images/homeIcon.png")}
                    />
                    <Text
                      style={styles.textPanel}
                      onPress={() => this.props.navigation.push("Intro")}
                    >
                      Home
                    </Text>
                  </View>
                  <View
                    style={{
                      flex: 1,
                      flexDirection: "row",
                      alignItems: "center",
                      height: responsiveHeight(10)
                    }}
                  >
                    <Image
                      style={styles.iconPanel}
                      source={require("../../src/images/logout.png")}
                    />
                    <Text
                      style={styles.textPanel}
                      onPress={() => this.onLogout()}
                    >
                      Cerrar sesión
                    </Text>
                  </View>
                  <Text
                    style={styles.textPanelHelp}
                    onPress={() =>
                      Linking.openURL(
                        "https://api.whatsapp.com/send?phone=593987700909&text=Bienvenido,%20en%20qu%C3%A9%20podemos%20ayudarte.%20Atentamente%20IMPTEK"
                      )
                    }
                  >
                    ¿Necesitas contactarte con nosotros?
                  </Text>
                  <View
                    style={{
                      flex: 1,
                      flexDirection: "row",
                      alignItems: "center",
                      height: responsiveHeight(20)
                    }}
                  >
                    <Image
                      style={styles.iconPanel}
                      source={require("../../src/images/mail.png")}
                    />
                    <Text
                      style={styles.textPanel}
                      onPress={() => Linking.openURL("mailto:info@imptek.com")}
                    >
                      info@imptek.com
                    </Text>
                  </View>
                  <View
                    style={{
                      flex: 1,
                      flexDirection: "row",
                      alignItems: "center",
                      height: responsiveHeight(20)
                    }}
                  >
                    <Image
                      style={styles.iconPanel}
                      source={require("../../src/images/whatsapp.png")}
                    />
                    <Text
                      style={styles.textPanel}
                      onPress={() =>
                        Linking.openURL(
                          "https://api.whatsapp.com/send?phone=593987700909&text=Bienvenido,%20en%20qu%C3%A9%20podemos%20ayudarte.%20Atentamente%20IMPTEK"
                        )
                      }
                    >
                      Whatsapp
                    </Text>
                  </View>
                </View>
              </View>
            )}
          />
        </SafeAreaView>
      );
    } else {
      return (
        <SafeAreaView style={styles.parentcontainer}>
          <Image
            style={styles.logo}
            source={require("../../src/images/logoimptek.png")}
            underlayColor="white"
          />
          <Text style={styles.text}>¿Qué deseas{"\n"}hacer hoy?</Text>
          <View style={styles.line} />
          <Text style={styles.textA}>actividades</Text>

          <CardView
            style={styles.cardContainer}
            cardElevation={7}
            cardMaxElevation={0}
            cornerRadius={5}
          >
            <View>
              <TouchableOpacity
                onPress={() => this.props.navigation.push("Loading")}
              >
                <Image
                  style={styles.card}
                  source={require("../../src/images/9convencion.png")}
                />
              </TouchableOpacity>
            </View>
          </CardView>
          <SlidingPanel
            panelPosition="top"
            allowDragging={false}
            headerLayoutHeight={responsiveHeight(40)}
            headerLayout={() => (
              <View style={styles.container}>
                <ImageBackground
                  style={styles.menuBg}
                  source={require("../../src/images/menuBg.png")}
                >
                  <Animated.Image
                    style={[styles.icon, animatedStyle]}
                    source={require("../../src/images/menuicon.png")}
                  />
                </ImageBackground>
              </View>
            )}
            onAnimationStart={this._animateIcon}
            onAnimationStop={() => this.toggleHandle()}
            slidingPanelLayoutHeight={responsiveHeight(100)}
            slidingPanelLayout={() => (
              <View style={styles.slidingPanelLayoutStyle}>
                <View style={styles.textContainer}>
                  <Text style={styles.nameMenu}></Text>
                  <View style={styles.linePanel} />
                  <View
                    style={{
                      flex: 1,
                      flexDirection: "row",
                      alignItems: "center",
                      height: responsiveHeight(10)
                    }}
                  >
                    <Image
                      style={styles.iconPanel}
                      source={require("../../src/images/homeIcon.png")}
                    />
                    <Text
                      style={styles.textPanel}
                      onPress={() => this.props.navigation.push("Intro")}
                    >
                      Home
                    </Text>
                  </View>
                  <View
                    style={{
                      flex: 1,
                      flexDirection: "row",
                      alignItems: "center",
                      height: responsiveHeight(10)
                    }}
                  />
                  <Text
                    style={styles.textPanelHelp}
                    onPress={() =>
                      Linking.openURL(
                        "https://api.whatsapp.com/send?phone=593987700909&text=Bienvenido,%20en%20qu%C3%A9%20podemos%20ayudarte.%20Atentamente%20IMPTEK"
                      )
                    }
                  >
                    ¿Necesitas contactarte con nosotros?
                  </Text>
                  <View
                    style={{
                      flex: 1,
                      flexDirection: "row",
                      alignItems: "center",
                      height: responsiveHeight(20)
                    }}
                  >
                    <Image
                      style={styles.iconPanel}
                      source={require("../../src/images/mail.png")}
                    />
                    <Text
                      style={styles.textPanel}
                      onPress={() => Linking.openURL("mailto:info@imptek.com")}
                    >
                      info@imptek.com
                    </Text>
                  </View>
                  <View
                    style={{
                      flex: 1,
                      flexDirection: "row",
                      alignItems: "center",
                      height: responsiveHeight(20)
                    }}
                  >
                    <Image
                      style={styles.iconPanel}
                      source={require("../../src/images/whatsapp.png")}
                    />
                    <Text
                      style={styles.textPanel}
                      onPress={() =>
                        Linking.openURL(
                          "https://api.whatsapp.com/send?phone=593987700909&text=Bienvenido,%20en%20qu%C3%A9%20podemos%20ayudarte.%20Atentamente%20IMPTEK"
                        )
                      }
                    >
                      Whatsapp
                    </Text>
                  </View>
                </View>
              </View>
            )}
          />
        </SafeAreaView>
      );
    }
  }
}

const styles = StyleSheet.create({
  parentcontainer: {
    flex: 1,
    backgroundColor: "#9B9A9A",
    width: responsiveWidth(100)
  },
  cardMenu: {
    marginTop: responsiveHeight(20),
    backgroundColor: "#9B9A9A"
  },
  logo: {
    left: responsiveWidth(20),
    width: responsiveWidth(25),
    height: Math.round((responsiveWidth(25) * 1) / 2.3),
    resizeMode: "contain",
    zIndex: 5,
    elevation: 25,
    marginTop: responsiveHeight(1)
  },
  text: {
    fontSize: responsiveFontSize(3),
    color: "#fff",
    marginLeft: responsiveWidth(5),
    marginTop: responsiveHeight(20),
    textAlign: "left"
  },
  line: {
    borderBottomColor: "white",
    borderBottomWidth: 1,
    width: responsiveWidth(80)
  },
  textA: {
    marginLeft: responsiveWidth(5),
    color: "#fff",
    fontSize: responsiveFontSize(2)
  },
  cardContainer: {
    width: responsiveWidth(90),
    height: Math.round((responsiveWidth(90) * 1) / 1.92),
    marginLeft: "auto",
    marginRight: "auto",
    zIndex: 0,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 12
    },
    shadowOpacity: 0.58,
    shadowRadius: 16.0,
    elevation: 20,
    marginTop: responsiveHeight(3)
  },
  user: {
    alignItems: "center",
    paddingTop: responsiveHeight(20)
  },
  card: {
    resizeMode: "contain",
    width: responsiveWidth(90),
    height: Math.round((responsiveWidth(90) * 1) / 1.92),
    alignItems: "center",
    zIndex: 0
  },
  container: {
    height: Math.round((responsiveWidth(100) * 1) / 1.95),
    justifyContent: "center",
    alignItems: "center",
    elevation: 24
  },
  menuBg: {
    width: responsiveWidth(100),
    height: Math.round((responsiveWidth(100) * 1) / 1.95),
    resizeMode: "contain",
    zIndex: 0
  },
  icon: {
    width: responsiveWidth(10),
    height: responsiveHeight(10),
    resizeMode: "contain",
    marginLeft: "auto",
    marginRight: responsiveWidth(20),
    marginTop: "auto",
    marginBottom: responsiveHeight(1)
  },
  slidingPanelLayoutStyle: {
    height: responsiveHeight(100),
    width: responsiveWidth(100),
    backgroundColor: "#00a7e1",
    justifyContent: "center",
    textAlign: "left",
    alignItems: "center",
    elevation: 24
  },
  textContainer: {
    width: responsiveWidth(90),
    marginTop: responsiveHeight(60),
    justifyContent: "flex-start",
    height: responsiveHeight(40),
    alignItems: "center"
  },
  textPanel: {
    fontSize: responsiveFontSize(2),
    color: "#fff",
    width: responsiveWidth(50)
  },
  textPanelHelp: {
    paddingTop: responsiveHeight(2),
    fontSize: responsiveFontSize(2),
    color: "#CDDB00",
    width: responsiveWidth(90),
    textAlign: "center",
    paddingBottom: responsiveHeight(4)
  },
  linePanel: {
    borderBottomColor: "white",
    borderBottomWidth: 2,
    width: responsiveWidth(80)
  },
  iconPanel: {
    flex: 2,
    height: responsiveHeight(2),
    resizeMode: "contain",
    backgroundColor: "#00a7e1"
  },
  containerPanel: {
    flex: 2,
    flexDirection: "row"
  },
  avatarMenu: {
    height: responsiveHeight(20),
    width: responsiveHeight(20)
  },
  nameMenu: {
    color: "#FFF",
    fontSize: responsiveFontSize(3),
    textAlign: "center"
  }
});
