import React from "react";
import {
  Alert,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View
} from "react-native";
import {
  responsiveFontSize,
  responsiveHeight,
  responsiveWidth
} from "react-native-responsive-dimensions";
export default class EventsDScreen extends React.Component {
  static navigationOptions = {
    title: "Visita técnica",
    headerStyle: {
      backgroundColor: "#20A5DC"
    },
    headerTitleStyle: {
      color: "white"
    }
  };

  render() {
    return (
      <ScrollView contentContainerStyle={styles.fullContainer} bounces={false}>
        <View style={styles.cardContainer1}>
          <TouchableWithoutFeedback>
            <Image
              style={styles.card1}
              source={require("../../src/images/V0830.png")}
            />
          </TouchableWithoutFeedback>
        </View>
        <View style={styles.cardContainer1}>
          <TouchableWithoutFeedback>
            <Image
              style={styles.card1}
              source={require("../../src/images/V0930.png")}
            />
          </TouchableWithoutFeedback>
        </View>
        <View style={styles.cardContainer1}>
          <TouchableWithoutFeedback>
            <Image
              style={styles.card1}
              source={require("../../src/images/V1000.png")}
            />
          </TouchableWithoutFeedback>
        </View>
        <View style={styles.cardContainerf}>
          <TouchableWithoutFeedback>
            <Image
              style={styles.card1}
              source={require("../../src/images/V1130.png")}
            />
          </TouchableWithoutFeedback>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  fullContainer: {
    flex: 1,
    alignItems: "center",
    backgroundColor: "#9C9B9B",
    width: responsiveWidth(100),
    height: responsiveHeight(100)
  },
  cardContainer1: {
    marginTop: responsiveHeight(2),
    width: responsiveWidth(90),
    height: Math.round((responsiveWidth(90) * 1) / 2.42),
    justifyContent: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 12
    },
    shadowOpacity: 0.58,
    shadowRadius: 16.0,
    elevation: 20
  },
  card1: {
    resizeMode: "contain",
    width: responsiveWidth(90),
    height: Math.round((responsiveWidth(90) * 1) / 2.42),
    justifyContent: "center"
  },
  cardContainerf: {
    marginTop: responsiveHeight(2),
    width: responsiveWidth(90),
    height: Math.round((responsiveWidth(90) * 1) / 2.42),
    justifyContent: "center",
    marginBottom: responsiveHeight(5),
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 12
    },
    shadowOpacity: 0.58,
    shadowRadius: 16.0,
    elevation: 20
  }
});
