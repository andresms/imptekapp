import React from "react";
import { Alert, Text, Image, ScrollView } from "react-native";
import { Card, ListItem, Button, Icon } from "react-native-elements";

const users = [
  {
    name: "Ing. Edwin León",
    avatar:
      "https://s3.amazonaws.com/cms-dev.imptek.com/descargables/Recurso+12%403x-100.jpg",
    pdfA:
      "https://s3.amazonaws.com/cms-dev.imptek.com/descargables/Ecuaroofing_Presentaci%C3%B3n+Estructuras+Enterradas_1.pdf.pdf",
    subtitle: "Experiencias de la impermeabilización en Ecuador"
  },
  {
    name: "Ing. Danilo Luna",
    avatar:
      "https://s3.amazonaws.com/cms-dev.imptek.com/descargables/Recurso+12%403x-100.jpg",
    pdfA:
      "https://s3.amazonaws.com/cms-dev.imptek.com/descargables/DANIEL+LUNA+Presentacion+Convencion+Imptek.pdf",
    subtitle: "Sistema de asfaltado en frío"
  },
  {
    name: "Dr. Gabriel Hernández",
    avatar:
      "https://s3.amazonaws.com/cms-dev.imptek.com/descargables/Recurso+12%403x-100.jpg",
    pdfA:
      "https://s3.amazonaws.com/cms-dev.imptek.com/descargables/IMPTEK-PRESS-GRAL-001+de+002.pdf",
    subtitle: "Caracterización y uso de asfalto modificado en la vialidad 1/2"
  },
  {
    name: "Dr. Gabriel Hernández",
    avatar:
      "https://s3.amazonaws.com/cms-dev.imptek.com/descargables/Recurso+12%403x-100.jpg",
    pdfA:
      "https://s3.amazonaws.com/cms-dev.imptek.com/descargables/IMPTEK-PRESS-GRAL-002+de+002.pdf",
    subtitle: "Caracterización y uso de asfalto modificado en la vialidad 2/2"
  },
  {
    name: "Ing. Itza López",
    avatar:
      "https://s3.amazonaws.com/cms-dev.imptek.com/descargables/Recurso+12%403x-100.jpg",
    pdfA:
      "https://s3.amazonaws.com/cms-dev.imptek.com/descargables/presentacion+ITZA+1.pdf",
    subtitle: "Experiencias en las aplicaciones viales utilizando Polibrea"
  },
  {
    name: "Ing. Guillermo Loria Salazar",
    avatar:
      "https://s3.amazonaws.com/cms-dev.imptek.com/descargables/Recurso+12%403x-100.jpg",
    pdfA:
      "https://s3.amazonaws.com/cms-dev.imptek.com/descargables/Tecnicas+de+preservacion+de+pavimentos+26-4-2019.pdf",
    subtitle: "Mantenimiento vial"
  },
  {
    name: "Ing. José Luis Martinez",
    avatar:
      "https://s3.amazonaws.com/cms-dev.imptek.com/descargables/Recurso+12%403x-100.jpg",
    pdfA:
      "https://s3.amazonaws.com/cms-dev.imptek.com/descargables/IMPTEK++-++CHOVA++CONFERENCIA++MAYO++8%2C++2019.+(1).pdf",
    subtitle: "Sistemas de impermeabilización base acrÍlica"
  },
  {
    name: "Ing. Raymundo Benítez",
    avatar:
      "https://s3.amazonaws.com/cms-dev.imptek.com/descargables/Recurso+12%403x-100.jpg",
    pdfA:
      "https://s3.amazonaws.com/cms-dev.imptek.com/descargables/raymundo.pdf",
    subtitle: "Mezclas asfálticas en frío, Bases estabilizadas"
  },
  {
    name: "Roberto Protto",
    avatar:
      "https://s3.amazonaws.com/cms-dev.imptek.com/descargables/Recurso+12%403x-100.jpg",
    pdfA:
      "https://s3.amazonaws.com/cms-dev.imptek.com/descargables/power+point+Quito+2019.+Roberto+(1).pdf",
    subtitle: "Sistemas semiadheridos utilizando Polyvent"
  }
];

export default class MaterialScreen extends React.Component {
  static navigationOptions = {
    title: "Descargas",
    headerStyle: {
      backgroundColor: "#20A5DC"
    },
    headerTitleStyle: {
      color: "white"
    }
  };

  render() {
    return (
      <ScrollView>
        <Card containerStyle={{ padding: 0 }}>
          {users.map((u, i) => {
            return (
              <ListItem
                key={i}
                title={u.name}
                subtitle={u.subtitle}
                leftAvatar={{ source: { uri: u.avatar } }}
                onPress={() =>
                  this.props.navigation.push("PDF", { url: u.pdfA })
                }
              />
            );
          })}
        </Card>
      </ScrollView>
    );
  }
}
