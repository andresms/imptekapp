import React from "react";
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  SafeAreaView
} from "react-native";
import {
  responsiveFontSize,
  responsiveHeight,
  responsiveWidth
} from "react-native-responsive-dimensions";
import CardView from "react-native-cardview";
import { Card, ListItem, Button, Icon } from "react-native-elements";

const users = [
  {
    avatar: "https://s3.amazonaws.com/cms-dev.imptek.com/promo/nacional/1.png"
  },
  {
    avatar: "https://s3.amazonaws.com/cms-dev.imptek.com/promo/nacional/2.png"
  },
  {
    avatar: "https://s3.amazonaws.com/cms-dev.imptek.com/promo/nacional/3.png"
  },
  {
    avatar: "https://s3.amazonaws.com/cms-dev.imptek.com/promo/nacional/4.png"
  },
  {
    avatar: "https://s3.amazonaws.com/cms-dev.imptek.com/promo/nacional/5.png"
  }
];

export default class Promociones2Screen extends React.Component {
  static navigationOptions = {
    title: "Promociones y ofertas",
    headerStyle: {
      backgroundColor: "#20A5DC"
    },
    headerTitleStyle: {
      color: "white"
    }
  };

  render() {
    return (
      <ScrollView>
        <Card>
          {users.map((u, i) => {
            return (
              <View key={i} style={styles.listContainer}>
                <Image style={styles.image} source={{ uri: u.avatar }} />
              </View>
            );
          })}
        </Card>
      </ScrollView>
    );
  }
}
const styles = StyleSheet.create({
  totalContainer: {
    width: responsiveWidth(100),
    alignItems: "center"
  },
  image: {
    width: responsiveWidth(90),
    height: Math.round((responsiveWidth(90) * 1) / 1.61),
    marginRight: responsiveWidth(17),
    borderRadius: 10,
    marginTop: responsiveHeight(2)
  },
  listContainer: {
    width: responsiveWidth(100),
    alignItems: "center"
  },
  fullContainer: {
    backgroundColor: "#9C9B9B",
    width: responsiveWidth(100),
    height: responsiveHeight(100),
    alignItems: "center"
  },
  cardContainer1: {
    width: responsiveWidth(90),
    height: Math.round((responsiveWidth(90) * 1) / 2.42),
    marginLeft: "auto",
    marginRight: "auto",
    zIndex: 0,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 12
    },
    shadowOpacity: 0.58,
    shadowRadius: 16.0,
    elevation: 20,
    marginBottom: responsiveHeight(3)
  },
  cardContainerA: {
    width: responsiveWidth(90),
    height: Math.round((responsiveWidth(90) * 1) / 2.42),
    marginLeft: "auto",
    marginRight: "auto",
    zIndex: 0,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 12
    },
    shadowOpacity: 0.58,
    shadowRadius: 16.0,
    elevation: 20,
    marginBottom: responsiveHeight(3),
    marginTop: responsiveHeight(5)
  },
  card1: {
    resizeMode: "contain",
    width: responsiveWidth(89),
    height: Math.round((responsiveWidth(90) * 1) / 2.42)
  },
  cardContainerD: {
    width: responsiveWidth(90),
    height: Math.round((responsiveWidth(90) * 1) / 2.42),
    marginLeft: "auto",
    marginRight: "auto",
    zIndex: 0,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 12
    },
    shadowOpacity: 0.58,
    shadowRadius: 16.0,
    elevation: 20,
    marginBottom: responsiveHeight(3)
  },
  cardD: {
    resizeMode: "contain",
    width: responsiveWidth(90),
    height: Math.round((responsiveWidth(90) * 1) / 2.42)
  }
});
