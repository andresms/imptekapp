import React from "react";
import { Modal } from "react-native";
import ImageViewer from "react-native-image-zoom-viewer";

const images = [
  {
    url: "https://s3.amazonaws.com/cms-dev.imptek.com/memories/DSC03714.jpg"
  },
  {
    url: "https://s3.amazonaws.com/cms-dev.imptek.com/memories/DSC03698.jpg"
  },
  {
    url: "https://s3.amazonaws.com/cms-dev.imptek.com/memories/DSC03664.jpg"
  },
  {
    url: "https://s3.amazonaws.com/cms-dev.imptek.com/memories/DSC03480.jpg"
  },
  {
    url: "https://s3.amazonaws.com/cms-dev.imptek.com/memories/DSC03480.jpg"
  },
  {
    url: "https://s3.amazonaws.com/cms-dev.imptek.com/memories/DSC03392.jpg"
  },
  {
    url: "https://s3.amazonaws.com/cms-dev.imptek.com/memories/DSC03376.jpg"
  },
  {
    url: "https://s3.amazonaws.com/cms-dev.imptek.com/memories/DSC03370.jpg"
  },
  {
    url: "https://s3.amazonaws.com/cms-dev.imptek.com/memories/DSC03365.jpg"
  },
  {
    url: "https://s3.amazonaws.com/cms-dev.imptek.com/memories/DSC03363.jpg"
  },
  {
    url: "https://s3.amazonaws.com/cms-dev.imptek.com/memories/DSC03351.jpg"
  },
  {
    url: "https://s3.amazonaws.com/cms-dev.imptek.com/memories/DSC03344.jpg"
  },
  {
    url: "https://s3.amazonaws.com/cms-dev.imptek.com/memories/DSC03336.jpg"
  },
  {
    url: "https://s3.amazonaws.com/cms-dev.imptek.com/memories/DSC03335.jpg"
  },
  {
    url: "https://s3.amazonaws.com/cms-dev.imptek.com/memories/DSC03128.jpg"
  },
  {
    url: "https://s3.amazonaws.com/cms-dev.imptek.com/memories/DSC02155.jpg"
  },
  {
    url: "https://s3.amazonaws.com/cms-dev.imptek.com/memories/DSC02006.jpg"
  },
  {
    url: "https://s3.amazonaws.com/cms-dev.imptek.com/memories/DSC01912.jpg"
  },
  {
    url: "https://s3.amazonaws.com/cms-dev.imptek.com/memories/DSC01870.jpg"
  },
  {
    url: "https://s3.amazonaws.com/cms-dev.imptek.com/memories/9.jpg"
  },
  {
    url: "https://s3.amazonaws.com/cms-dev.imptek.com/memories/IMG_4927.jpg"
  },
  {
    url: "https://s3.amazonaws.com/cms-dev.imptek.com/memories/IMG_5009.jpg"
  },
  {
    url: "https://s3.amazonaws.com/cms-dev.imptek.com/memories/IMG_5074.jpg"
  },
  {
    url: "https://s3.amazonaws.com/cms-dev.imptek.com/memories/IMG_5088.jpg"
  },
  {
    url: "https://s3.amazonaws.com/cms-dev.imptek.com/memories/IMG_5138.jpg"
  },
  {
    url: "https://s3.amazonaws.com/cms-dev.imptek.com/memories/IMG_5144.jpg"
  },
  {
    url: "https://s3.amazonaws.com/cms-dev.imptek.com/memories/IMG_5163.jpg"
  },
  {
    url: "https://s3.amazonaws.com/cms-dev.imptek.com/memories/IMG_5172.jpg"
  },
  {
    url: "https://s3.amazonaws.com/cms-dev.imptek.com/memories/IMG_5272.jpg"
  },
  {
    url: "https://s3.amazonaws.com/cms-dev.imptek.com/memories/IMG_5338.jpg"
  },
  {
    url: "https://s3.amazonaws.com/cms-dev.imptek.com/memories/IMG_5695.jpg"
  },
  {
    url: "https://s3.amazonaws.com/cms-dev.imptek.com/memories/IMG_5799.jpg"
  },
  {
    url: "https://s3.amazonaws.com/cms-dev.imptek.com/memories/IMG_6219.jpg"
  }
];

export default class MemoriesScreen extends React.Component {
  static navigationOptions = {
    title: "Memorias",
    headerStyle: {
      backgroundColor: "#20A5DC"
    },
    headerTitleStyle: {
      color: "white"
    }
  };

  constructor(props) {
    super(props);
  }

  render() {
    return <ImageViewer imageUrls={images} />;
  }
}
