import React from "react";
import {
  Alert,
  Image,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  Button,
  View,
  Linking
} from "react-native";
import {
  responsiveFontSize,
  responsiveHeight,
  responsiveWidth
} from "react-native-responsive-dimensions";
import StarRating from "react-native-star-rating";
import ActionButton from "react-native-action-button";
import Icon from "react-native-vector-icons/Ionicons";

export default class ProfileScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      starCount: 3.5,
      disabled: false
    };
  }

  onStarRatingPress(rating) {
    this.setState({
      starCount: rating,
      disabled: true
    });
    Alert.alert("Gracias", "Hemos recibido tu calificación.");
  }

  static navigationOptions = {
    title: "Perfil",
    headerStyle: {
      backgroundColor: "#00A7E1"
    },
    headerTitleStyle: {
      color: "white"
    }
  };

  render() {
    const { navigation } = this.props;
    const name = navigation.getParam("name", "");
    const country = navigation.getParam("country", "");
    const describe = navigation.getParam("describe", "");
    return (
      <ScrollView contentContainerStyle={styles.container} bounces={false}>
        <View style={styles.bg}>
          <Image
            style={styles.bgImage}
            source={require("../../src/images/perfilBg.png")}
          />
        </View>
        <View style={styles.parent}>
          <View>
            <Text style={styles.name}>{name}</Text>
            <Text style={styles.country}>{country}</Text>
            <Text style={styles.country}>Califica el evento:</Text>
            <StarRating
              starSize={22}
              containerStyle={styles.rating}
              disabled={this.state.disabled}
              maxStars={5}
              fullStarColor={"#CDDB00"}
              rating={this.state.starCount}
              selectedStar={rating => this.onStarRatingPress(rating)}
            />
            <View style={styles.line} />
            <View style={styles.describeContainer}>
              <Text style={styles.describe}>{describe}</Text>
            </View>
          </View>
        </View>
        <ActionButton buttonColor="rgba(205,219,0,1)">
          <ActionButton.Item
            buttonColor="#FFF"
            title="Escribir pregunta"
            onPress={() =>
              Linking.openURL(
                "https://api.whatsapp.com/send?phone=593987700909"
              )
            }
          >
            <Icon
              name="md-create"
              color="#3f3f3f"
              style={styles.actionButtonIcon}
            />
          </ActionButton.Item>
        </ActionButton>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  rating: {
    marginTop: responsiveHeight(2),
    width: responsiveWidth(30)
  },
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "flex-start",
    backgroundColor: "#00A7E1",
    width: responsiveWidth(100)
  },
  bg: {
    position: "absolute",
    width: responsiveWidth(100),
    height: responsiveHeight(50),
    zIndex: 0
  },
  bgImage: {
    width: responsiveWidth(100),
    height: responsiveHeight(30),
    resizeMode: "contain",
    zIndex: 0
  },
  name: {
    color: "white",
    marginTop: responsiveHeight(3),
    fontSize: responsiveFontSize(3),
    textAlign: "left"
  },
  country: {
    color: "white",
    marginTop: responsiveHeight(2),
    fontSize: responsiveFontSize(2),
    textAlign: "left"
  },
  parent: {
    width: responsiveWidth(90)
  },
  line: {
    marginTop: responsiveHeight(3),
    borderBottomColor: "white",
    borderBottomWidth: 2,
    width: responsiveWidth(90)
  },
  describe: {
    color: "white",
    marginTop: responsiveHeight(5),
    textAlign: "justify",
    fontSize: responsiveFontSize(2)
  },
  describeContainer: {}
});
