import React, { Component } from "react";
import {
  Text,
  SafeAreaView,
  View,
  StyleSheet,
  TextInput,
  AsyncStorage,
  Alert,
  BackHandler
} from "react-native";
import {
  responsiveHeight,
  responsiveWidth,
  responsiveFontSize
} from "react-native-responsive-dimensions";
import KeyboardSpacer from "react-native-keyboard-spacer";
import Button from "apsl-react-native-button";
import * as firebase from "firebase";

export default class LoginScreen extends Component {
  static navigationOptions = {
    title: "Inicio de sesión",
    headerLeft: null
  };

  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      role: 0
    };
    this.login = this.login.bind(this);
  }
  componentDidMount() {
    BackHandler.addEventListener("hardwareBackPress", this.handleBackButton);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.handleBackButton);
  }

  handleBackButton = () => {
    return true;
  };

  async login() {
    try {
      firebase.auth.Auth.Persistence.LOCAL;
      await firebase
        .auth()
        .signInWithEmailAndPassword(this.state.email, this.state.password)
        .then(() => this.props.navigation.push("HomeExpo"));
    } catch (e) {
      Alert.alert("Error", e.toString());
    }
  }

  render() {
    return (
      <SafeAreaView
        style={styles.container}
        keyboardShouldPersistTaps={"handled"}
      >
        <View style={styles.textContainerH2}>
          <Text style={styles.textLoginH1}>Bienvenido de nuevo,</Text>
          <Text style={styles.textLoginH2}>Inicia sesión usando</Text>
          <Text style={styles.textLoginH2}>tu correo</Text>
        </View>

        <TextInput
          style={styles.emailInput}
          placeholder={"Correo Electrónico"}
          placeholderTextColor={"#666666"}
          value={this.state.email}
          textContentType={"emailAddress"}
          blurOnSubmit={true}
          clearTextOnFocus={true}
          autoCapitalize={"none"}
          autoCorrect={false}
          onChangeText={text => this.setState({ email: text })}
        />

        <TextInput
          style={styles.emailInput}
          onChangeText={text => this.setState({ password: text })}
          placeholder={"Contraseña"}
          blurOnSubmit={true}
          secureTextEntry={true}
          clearTextOnFocus={true}
          placeholderTextColor={"#666666"}
        />
        <View
          style={{ width: responsiveWidth(80), marginTop: responsiveHeight(2) }}
        >
          <Text
            style={{
              textAlign: "right",
              color: "#808080",
              marginTop: responsiveHeight(1)
            }}
            onPress={() => this.props.navigation.push("Recover")}
          >
            ¿Olvidaste tu contraseña?
          </Text>

          <Button style={styles.loginButton} onPress={this.login}>
            <Text style={styles.loginText}>Acceder</Text>
          </Button>
          <KeyboardSpacer />

          <Text style={{ marginTop: responsiveHeight(7) }}>
            ¿Nuevo usuario?
          </Text>
          <Button
            buttonStyle={styles.registryButton}
            onPress={() => this.props.navigation.push("Registry")}
          >
            <Text style={styles.registryText}>Regístrate</Text>
          </Button>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#f9f9f9"
  },
  textContainerH2: {
    width: responsiveWidth(80)
  },
  textLoginH1: {
    color: "#666666",
    fontSize: responsiveFontSize(4)
  },
  textLoginH2: {
    color: "#666666",
    fontSize: responsiveFontSize(2),
    textAlign: "left"
  },
  emailInput: {
    marginTop: responsiveHeight(4),
    borderColor: "#00A7E1",
    borderWidth: 2,
    width: responsiveWidth(80),
    height: responsiveHeight(6),
    borderRadius: 7,
    alignItems: "center",
    paddingLeft: responsiveWidth(5),
    backgroundColor: "#FFF"
  },
  loginText: {
    color: "white"
  },
  registryButton: {
    marginTop: responsiveHeight(1),
    backgroundColor: "#CDDB00",
    width: responsiveWidth(30),
    borderColor: "#f9f9f9"
  },
  registryText: {
    color: "white"
  }
});
