import React, { Component } from "react";
import {
  Text,
  SafeAreaView,
  View,
  StyleSheet,
  TextInput,
  AsyncStorage,
  Alert,
  Button,
  Image
} from "react-native";
import {
  responsiveHeight,
  responsiveWidth,
  responsiveFontSize
} from "react-native-responsive-dimensions";
import * as firebase from "firebase";

export default class MiddleScreen extends Component {
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);
  }
  componentDidMount() {}

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.view}>
          <Image
            style={styles.image}
            source={require("../../src/images/logoimptek.png")}
          ></Image>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    width: responsiveWidth(100),
    height: responsiveHeight(100),
    backgroundColor: "#9C9B9B"
  },
  view: {
    width: responsiveWidth(30),
    height: responsiveHeight(30)
  },
  image: {
    width: responsiveWidth(30),
    height: responsiveHeight(30),
    resizeMode: "contain"
  }
});
