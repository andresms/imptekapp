import * as firebase from "firebase";

class Firebase {
  static init() {
    firebase.initializeApp({
      apiKey: "AIzaSyAtgoVhWIBa0hnVd39zbzzkalm2-HfzOKM",
      authDomain: "imptekconvencion.firebaseapp.com",
      databaseURL: "https://imptekconvencion.firebaseio.com",
      projectId: "imptekconvencion",
      storageBucket: "imptekconvencion.appspot.com",
      messagingSenderId: "718305610421",
      appId: "1:718305610421:web:76ef71a26990c7d9"
    });
  }
}

module.exports = Firebase;
