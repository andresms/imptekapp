import { createStackNavigator, createAppContainer } from "react-navigation";

import IntroScreen from "./components/Intro";
import LoginScreen from "./components/Login";
import HomeExpoScreen from "./screens/Convencion/HomeExpo";
import RegistryScreen from "./components/Registry";
import RecoverScreen from "./components/Recover";
import MemoriesScreen from "./components/Memories";
import VideoScreen from "./screens/Convencion/Video";
import ProfileScreen from "./components/Profile";
import CenaScreen from "./components/Cena";
import VisitaScreen from "./components/Visita";
import PromoScreen from "./components/Promos";
import MaterialScreen from "./components/Material";
import PDFScreen from "./components/PDF";
import Promociones1Screen from "./components/Promociones1";
import Promociones2Screen from "./components/Promociones2";
import Promociones3Screen from "./components/Promociones3";
import LoadingScreen from "./components/Loading";
import MiddleScreen from "./components/Middle";
import MemoriasScreen from "./screens/Convencion/Memorias";
import MaterialDescargableScreen from "./screens/Convencion/MaterialDescargable";

const Navigator = createStackNavigator(
  {
    Intro: IntroScreen,
    Login: LoginScreen,
    HomeExpo: HomeExpoScreen,
    Registry: RegistryScreen,
    Recover: RecoverScreen,
    Memories: MemoriesScreen,
    Video: VideoScreen,
    Profile: ProfileScreen,
    Cena: CenaScreen,
    Visita: VisitaScreen,
    Promo: PromoScreen,
    Material: MaterialScreen,
    PDF: PDFScreen,
    Promociones1: Promociones1Screen,
    Promociones2: Promociones2Screen,
    Promociones3: Promociones3Screen,
    Loading: LoadingScreen,
    Middle: MiddleScreen,
    Memorias: MemoriasScreen,
    MaterialDescargable: MaterialDescargableScreen
  },
  {
    initialRouteName: "Intro"
  }
);

export default createAppContainer(Navigator);
