import React, { Component } from "react";
import {
  Button,
  Text,
  View,
  ScrollView,
  StyleSheet,
  Image,
  TouchableOpacity,
  Linking
} from "react-native";
import { Avatar } from "react-native-elements";
import ActionButton from "react-native-action-button";

import {
  responsiveFontSize,
  responsiveHeight,
  responsiveWidth
} from "react-native-responsive-dimensions";
import Icon from "react-native-vector-icons/index";

export default class InicioScreen extends React.Component {
  static navigationOptions = {
    title: "Home",
    headerLeft: null,
    headerStyle: {
      backgroundColor: "#20A5DC"
    },
    headerTitleStyle: {
      color: "white"
    }
  };
  render() {
    return (
      <ScrollView style={styles.bgHome}>
        <View style={styles.avatarContainer}>
          <Avatar
            size="medium"
            rounded
            source={{
              uri:
                "https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg"
            }}
          />
          <View style={styles.avatarInfo}>
            <Text>firstname lastname</Text>
            <Text>lorem ipsum</Text>
          </View>
        </View>
        <View style={styles.lineAvatar} />
        <View style={styles.textHomeContainer}>
          {/*<Text style={styles.textA}>¿Qué deseas{"\n"}hacer hoy?</Text>*/}
          {/*<Text style={styles.textB}>descubre</Text>*/}
        </View>
        <TouchableOpacity>
          <Image
            style={styles.home01}
            source={require("../../src/images/home01.png")}
          />
        </TouchableOpacity>
        <View style={styles.textNewsContainer}>
          <Text style={styles.textC}>¿Humedad en las superficies</Text>
          <Text style={styles.textC}>de tus construcciones?</Text>
          <Text style={styles.textD}>Lorem ipsum</Text>
        </View>
        <View style={styles.line} />
        <TouchableOpacity>
          <Image
            style={styles.news}
            source={require("../../src/images/noticias.png")}
          />
        </TouchableOpacity>
        <View style={styles.line} />
        <ScrollView style={styles.horizontalScroll} horizontal={true}>
          <Image
            style={styles.slide01}
            source={require("../../src/images/superacryl.png")}
          />
          <Image
            style={styles.slide01}
            source={require("../../src/images/imprimante.png")}
          />
          <Image
            style={styles.slide01}
            source={require("../../src/images/superacryl.png")}
          />
          <Image
            style={styles.slide02}
            source={require("../../src/images/imprimante.png")}
          />
        </ScrollView>
      </ScrollView>
    );
  }
}
const styles = StyleSheet.create({
  bgHome: {
    flex: 1,
    backgroundColor: "white"
  },
  avatarContainer: {
    marginTop: responsiveHeight(2),
    marginLeft: responsiveWidth(5),
    width: responsiveHeight(90),
    flexDirection: "row",
    alignItems: "center"
  },
  avatarInfo: {
    marginLeft: responsiveWidth(4)
  },
  textHomeContainer: {
    marginTop: responsiveHeight(4),
    width: responsiveWidth(90),
    marginLeft: responsiveWidth(5)
  },
  textA: {
    color: "gray",
    fontSize: responsiveFontSize(4)
  },
  textB: {
    color: "gray",
    fontSize: responsiveFontSize(2)
  },
  textNewsContainer: {
    width: responsiveWidth(90),
    marginLeft: responsiveWidth(5)
  },
  textC: {
    color: "#333333",
    fontSize: responsiveFontSize(3)
  },
  textD: {
    color: "#333333",
    fontSize: responsiveFontSize(2)
  },
  news: {
    marginTop: responsiveHeight(3),
    resizeMode: "contain",
    width: responsiveWidth(90),
    height: Math.round((responsiveWidth(90) * 1) / 2.4),
    marginLeft: responsiveWidth(5)
  },
  home01: {
    resizeMode: "contain",
    width: responsiveWidth(90),
    height: Math.round((responsiveWidth(90) * 1) / 2.21),
    marginLeft: responsiveWidth(5)
  },
  lineAvatar: {
    marginTop: responsiveHeight(1),
    borderBottomColor: "#f1f1f1",
    borderBottomWidth: 3,
    width: responsiveWidth(100)
  },
  line: {
    marginTop: responsiveHeight(3),
    borderBottomColor: "#f1f1f1",
    borderBottomWidth: 5,
    width: responsiveWidth(100)
  },
  horizontalScroll: {
    marginTop: responsiveHeight(3),
    marginBottom: responsiveHeight(3)
  },
  slide01: {
    resizeMode: "contain",
    width: responsiveWidth(45),
    height: Math.round((responsiveWidth(45) * 1) / 0.69),
    marginLeft: responsiveWidth(5)
  },
  slide02: {
    resizeMode: "contain",
    width: responsiveWidth(45),
    height: Math.round((responsiveWidth(45) * 1) / 0.69),
    marginLeft: responsiveWidth(5),
    marginRight: responsiveWidth(5)
  },
  actionButtonIcon: {}
});
