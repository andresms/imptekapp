import React, { Component } from "react";
import {
  Text,
  SafeAreaView,
  View,
  StyleSheet,
  TextInput,
  AsyncStorage,
  Alert,
  TouchableOpacity
} from "react-native";
import {
  responsiveHeight,
  responsiveWidth,
  responsiveFontSize
} from "react-native-responsive-dimensions";
import * as firebase from "firebase";
import { SocialIcon } from "react-native-elements/src/index";
import Ionicons from "react-native-vector-icons/Ionicons";

export default class UserLoginScreen extends Component {
  static navigationOptions = {
    header: null
  };

  validate = text => {
    console.log(text);
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(text) === false) {
      this.setState({ email: text, valid: false });
      return false;
    } else {
      this.setState({ email: text, valid: true });
    }
  };

  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      valid: false
    };
    this.login = this.login.bind(this);
  }
  async login() {
    try {
      firebase.auth.Auth.Persistence.LOCAL;
      await firebase
        .auth()
        .signInWithEmailAndPassword(this.state.email, this.state.password);
    } catch (e) {
      Alert.alert("Error", e.toString());
    }
  }

  render() {
    return (
      <SafeAreaView style={styles.bg}>
        <View>
          <Text style={styles.loginText}>Log In</Text>
        </View>
        <View style={styles.inputLoginInput}>
          <Ionicons
            style={styles.iconLogin}
            name={"md-mail"}
            size={25}
            color={"white"}
          />
          <TextInput
            style={styles.emailInput}
            placeholder={"Email Address"}
            placeholderTextColor={"#e8ede8"}
            selectionColor={"#cddb00"}
            textContentType={"emailAddress"}
            keyboardType={"email-address"}
            blurOnSubmit={true}
            clearTextOnFocus={true}
            autoCapitalize={"none"}
            autoCorrect={false}
            onChangeText={text => this.validate(text)}
            value={this.state.email}
          />
          <Ionicons
            style={styles.iconCheck}
            name={
              this.state.valid
                ? "md-checkmark-circle-outline"
                : "md-remove-circle-outline"
            }
            size={18}
            color={"#cddb00"}
          />
        </View>
        <View style={styles.inputLoginInput}>
          <Ionicons
            style={styles.iconLogin}
            name={"md-lock"}
            size={25}
            color={"white"}
          />
          <TextInput
            style={styles.passInput}
            placeholder={"Password"}
            placeholderTextColor={"#e8ede8"}
            selectionColor={"#cddb00"}
            blurOnSubmit={true}
            clearTextOnFocus={true}
            autoCapitalize={"none"}
            autoCorrect={false}
            secureTextEntry={true}
            onChangeText={text => this.setState({ password: text })}
          />
        </View>
        <TouchableOpacity onPress={this.login} disabled={!this.state.valid}>
          <View style={styles.buttonLogin}>
            <Text style={styles.buttonTextLogin}>Login to your account</Text>
          </View>
        </TouchableOpacity>
        <View style={styles.registerContainer}>
          <Text style={styles.registerLabel}>Eres nuevo? </Text>
          <Text
            style={styles.registerAction}
            onPress={() => this.props.navigation.navigate("Registro")}
          >
            {" "}
            regístrate
          </Text>
        </View>
        <SocialIcon
          style={styles.socialLogin}
          title="Facebook Login"
          button
          type="facebook"
        />
        <SocialIcon
          style={styles.socialLogin}
          title="Twitter Login"
          button
          type="twitter"
        />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  bg: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#0abdee"
  },
  loginText: {
    textAlign: "left",
    width: responsiveWidth(80),
    color: "white",
    fontSize: responsiveFontSize(3)
  },
  inputLoginInput: {
    marginTop: responsiveHeight(4),
    flexDirection: "row",
    alignItems: "center"
  },
  iconLogin: {
    width: responsiveWidth(10)
  },
  iconCheck: {
    width: responsiveWidth(5)
  },
  emailInput: {
    color: "white",
    width: responsiveWidth(65),
    height: responsiveHeight(6),
    paddingLeft: responsiveWidth(5),
    backgroundColor: "transparent",
    borderBottomWidth: responsiveWidth(0.1),
    borderBottomColor: "white"
  },
  passInput: {
    color: "white",
    width: responsiveWidth(65),
    height: responsiveHeight(6),
    paddingLeft: responsiveWidth(5),
    marginRight: responsiveWidth(5),
    backgroundColor: "transparent",
    borderBottomWidth: responsiveWidth(0.1),
    borderBottomColor: "white"
  },
  buttonLogin: {
    width: responsiveWidth(80),
    backgroundColor: "#cddb00",
    borderColor: "white",
    borderRadius: responsiveWidth(1),
    marginTop: responsiveHeight(3)
  },
  buttonTextLogin: {
    textAlign: "center",
    padding: responsiveFontSize(2),
    fontSize: responsiveFontSize(2)
  },
  registerContainer: {
    width: responsiveWidth(80),
    marginTop: responsiveHeight(2),
    flexDirection: "row",
    justifyContent: "flex-end",
    marginBottom: responsiveHeight(3)
  },
  registerLabel: {
    textAlign: "right",
    color: "#e8ede8"
  },
  registerAction: {
    textAlign: "right",
    color: "white"
  },
  socialLogin: {
    width: responsiveWidth(80)
  }
});
