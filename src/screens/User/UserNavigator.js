import { createStackNavigator, createAppContainer } from "react-navigation";

import UserLoginScreen from "./Login";
import RegistroScreen from "./Registro";

const Navigator = createStackNavigator(
  {
    Login: UserLoginScreen,
    Registro: RegistroScreen
  },
  {
    initialRouteName: "Login"
  }
);

export default createAppContainer(Navigator);
