import React from "react";
import {
  Text,
  SafeAreaView,
  View,
  TextInput,
  StyleSheet,
  Alert
} from "react-native";
import {
  responsiveFontSize,
  responsiveHeight,
  responsiveWidth
} from "react-native-responsive-dimensions";
import Button from "apsl-react-native-button";
import * as firebase from "firebase";
import Firebase from "../../firebase";

export default class RegistroScreen extends React.Component {
  static navigationOptions = {
    title: "Registro"
  };

  constructor(props) {
    super(props);
    this.state = {
      firstname: "",
      lastname: "",
      email: "",
      password: "",
      passwordConfirm: "",
      estado: ""
    };
    this.registry = this.registry.bind(this);
  }

  async registry() {
    try {
      firebase.auth.Auth.Persistence.LOCAL;
      await firebase
        .auth()
        .createUserWithEmailAndPassword(this.state.email, this.state.password)
        .then(() => this.props.navigation.push("Slide"));
    } catch (error) {
      Alert.alert("Error", error.toString());
    }
  }

  // componentWillMount(){
  //     firebase.initializeApp(
  //         {
  //             apiKey: "AIzaSyAtgoVhWIBa0hnVd39zbzzkalm2-HfzOKM",
  //             authDomain: "imptekconvencion.firebaseapp.com",
  //             databaseURL: "https://imptekconvencion.firebaseio.com",
  //             projectId: "imptekconvencion",
  //             storageBucket: "imptekconvencion.appspot.com",
  //             messagingSenderId: "718305610421",
  //         });
  // }

  // _Registry2() {
  //     if (this.state.password === this.state.passwordConfirm) {
  //
  //     fetch('http://api-dev.imptek.com:4900/api/v1/registry', {
  //         method: 'POST',
  //         headers: {
  //             Accept: 'application/json',
  //             'Content-Type': 'application/json',
  //         },
  //         body: JSON.stringify({
  //             firstname: this.state.firstname,
  //             lastname: this.state.lastname,
  //             email: this.state.email,
  //             password: this.state.password
  //         })
  //     })
  //         .then(response => {
  //             if (response.status === 201) {
  //                 Alert.alert('Registro Exitoso');
  //                 this.props.navigation.navigate('Slide');
  //             } else {
  //                 Alert.alert('Error al registrar el usuario');
  //             }
  //         })
  //         .then(response => {
  //             console.debug(response);
  //         }).catch(error => {
  //         console.error(error);
  //     });
  //     } else {
  //         Alert.alert('Las contraseñas no coinciden')
  //     }
  // }

  // storeToken(responseData) {
  //     AsyncStorage.setItem(ACCESS_TOKEN, responseData, (err) => {
  //         if (err) {
  //             console.log("an error");
  //             throw err;
  //         }
  //         console.log("success");
  //     }).catch((err) => {
  //         console.log("error is: " + err);
  //     });
  // }

  // async _Registry() {
  //     if (this.state.password != this.state.passwordConfirm) {
  //         Alert.alert("Error", "Las contraseñas no coinciden")
  //     } else {
  //
  //         try {
  //             let response = await fetch('http://api-dev.imptek.com:4900/api/v1/registry', {
  //                 method: 'POST',
  //                 headers: {
  //                     'Accept': 'application/json',
  //                     'Content-Type': 'application/json',
  //                 },
  //                 body: JSON.stringify({
  //                     firstname: this.state.firstname,
  //                     lastname: this.state.lastname,
  //                     email: this.state.email,
  //                     password: this.state.password
  //                 })
  //             });
  //             let res = await response.text();
  //             if (response.status >= 200 && response.status < 300) {
  //                 //Handle success
  //                 let accessToken = res;
  //                 console.log(accessToken);
  //                 //On success we will store the access_token in the AsyncStorage
  //                 this.storeToken(accessToken);
  //                 Alert.alert("Bienvenido", "Registro exitoso")
  //                 // this.redirect('home');
  //                 this.state.email = '';
  //                 this.state.password = '';
  //                 this.props.navigation.push('Slide');
  //             } else {
  //                 //Handle error
  //                 // let error = res;
  //                 // throw error;
  //                 Alert.alert('Error', 'El usuario ya se encuentra registrado.');
  //                 this.state.email = '';
  //                 this.state.password = '';
  //             }
  //         } catch (error) {
  //             // this.setState({error: error});
  //             console.log("error ");
  //             Alert.alert("Error", "Algo salío mal, intenta nuevamente.")
  //             // this.setState({showProgress: false});
  //         }
  //     }
  // }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.textContainerH2}>
          <Text style={styles.textLoginH1}>Ingresa,</Text>
          <Text style={styles.textLoginH2}>tu correo</Text>
          <Text style={styles.textLoginH2}>y contraseña</Text>
        </View>

        <TextInput
          style={styles.emailInput}
          placeholder={"Nombre"}
          placeholderTextColor={"#666666"}
          value={this.state.firstname}
          textContentType={"emailAddress"}
          blurOnSubmit={true}
          clearTextOnFocus={true}
          autoCapitalize={"none"}
          autoCorrect={false}
          onChangeText={text => this.setState({ firstname: text })}
        />
        <TextInput
          style={styles.emailInput}
          placeholder={"Apellido"}
          placeholderTextColor={"#666666"}
          value={this.state.lastname}
          textContentType={"emailAddress"}
          blurOnSubmit={true}
          clearTextOnFocus={true}
          autoCapitalize={"none"}
          autoCorrect={false}
          onChangeText={text => this.setState({ lastname: text })}
        />
        <TextInput
          style={styles.emailInput}
          placeholder={"Correo Electrónico"}
          placeholderTextColor={"#666666"}
          value={this.state.email}
          textContentType={"emailAddress"}
          blurOnSubmit={true}
          clearTextOnFocus={true}
          autoCapitalize={"none"}
          autoCorrect={false}
          onChangeText={text => this.setState({ email: text })}
        />
        <TextInput
          style={styles.emailInput}
          onChangeText={text => this.setState({ password: text })}
          value={this.state.password}
          placeholder={"Contraseña"}
          blurOnSubmit={true}
          secureTextEntry={true}
          clearTextOnFocus={true}
          placeholderTextColor={"#666666"}
        />
        <TextInput
          style={styles.emailInput}
          onChangeText={text => this.setState({ passwordConfirm: text })}
          placeholder={"Confirma tu contrasena"}
          blurOnSubmit={true}
          secureTextEntry={true}
          clearTextOnFocus={true}
          placeholderTextColor={"#666666"}
        />
        <View
          style={{ width: responsiveWidth(80), marginTop: responsiveHeight(2) }}
        >
          <Button
            style={styles.registryB}
            color="#00A7E1"
            onPress={this.registry}
          >
            <Text style={styles.textB}>Registrarme</Text>
          </Button>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    backgroundColor: "#f9f9f9"
  },
  textContainerH2: {
    width: responsiveWidth(80)
  },
  textLoginH1: {
    color: "#666666",
    fontSize: responsiveFontSize(4)
  },
  textLoginH2: {
    color: "#666666",
    fontSize: responsiveFontSize(2),
    textAlign: "left"
  },
  emailInput: {
    marginTop: responsiveHeight(2),
    borderColor: "#00A7E1",
    borderWidth: 2,
    width: responsiveWidth(80),
    height: responsiveHeight(6),
    borderRadius: 7,
    alignItems: "center",
    paddingLeft: responsiveWidth(5),
    backgroundColor: "#FFF"
  },
  registryB: {
    backgroundColor: "#00A7E1",
    borderColor: "#f9f9f9"
  },
  textB: {
    color: "white"
  }
});
