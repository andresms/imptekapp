import React, { Component } from "react";
import {
  Button,
  Text,
  View,
  ScrollView,
  TextInput,
  Linking,
  StyleSheet,
  ImageBackground,
  Image,
  TouchableOpacity
} from "react-native";
import {
  responsiveFontSize,
  responsiveHeight,
  responsiveWidth
} from "react-native-responsive-dimensions";
import ActionButton from "react-native-action-button";
import Icon from "react-native-vector-icons/Ionicons";

export default class ProcesoScreen extends React.Component {
  static navigationOptions = ({ navigation }) => ({
    title: `${navigation.state.params.title}`,
    headerStyle: {
      backgroundColor: "#20A5DC"
    },
    headerTitleStyle: {
      color: "white"
    }
  });
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      monocapaImperpol: false,
      monocapaImperglass: false,
      bicapaImperpol: false,
      bicapaImperglass: false,
      superacryl: false,
      superacrylSupermalla: false,
      cubiertas: false,
      number: 0,
      imperpolCantidad: 0,
      imperpolPrecio: 0,
      polibreaCantidad: 0,
      polibreaPrecio: 0,
      imperlasticCantidad: 0,
      imperlasticPrecio: 0,
      imperglassCantidad: 0,
      imperglassPrecio: 0,
      totalM: 0,
      metroM: 0,
      totalMI: 0,
      metroMI: 0
    };
    this.componentDidMount = this.componentDidMount.bind(this);
    this.handleChangeNumber = this.handleChangeNumber.bind(this);
  }
  componentDidMount() {
    const { navigation } = this.props;
    switch (`${navigation.state.params.title}`) {
      case "Sistemas monocapa Imperpol":
        this.setState({
          monocapaImperpol: true
        });
        break;
      case "Sistemas monocapa Imperglass":
        this.setState({
          monocapaImperglass: true
        });
        break;
      case "Sistemas bicapa Imperpol":
        this.setState({
          bicapaImperpol: true
        });
        break;
      case "Sistemas bicapa Imperglass":
        this.setState({
          bicapaImperglass: true
        });
        break;
      case "SUPERACRYL SUPERMALLA":
        this.setState({
          superacrylSupermalla: true
        });
        break;
      case "SUPERACRYL":
        this.setState({
          superacryl: true
        });
        break;
      case "Cubiertas ajardinadas":
        this.setState({
          cubiertas: true
        });
        break;
    }
  }

  handleChangeNumber(number, value) {
    let imperpol = Math.ceil(value / 9);
    let imperpolP = imperpol * 59.76;
    this.setState({ imperpolCantidad: imperpol });
    this.setState({ imperpolPrecio: imperpolP });
    let polibrea = Math.ceil(imperpol / 8);
    let polibreaP = polibrea * 27.5;
    this.setState({ polibreaCantidad: polibrea });
    this.setState({ polibreaPrecio: polibreaP });
    let imperlastic = Math.ceil(value / 110);
    console.log(imperlastic);
    let imperlasticP = imperlastic * 25.61;
    console.log(imperlasticP);
    this.setState({ imperlasticCantidad: imperlastic });
    this.setState({ imperlasticPrecio: imperlasticP });
    let totalMonocapa = imperpolP + polibreaP + imperlasticP;
    let metroMonocapa = totalMonocapa / value;
    this.setState({ totalM: totalMonocapa.toFixed(2) });
    this.setState({ metroM: metroMonocapa.toFixed(2) });
    let imperglass = Math.ceil(value / 9);
    let imperglassP = imperglass * 52.2;
    this.setState({ imperglassCantidad: imperglass });
    this.setState({ imperglassPrecio: imperglassP.toFixed(2) });
    let superk = Math.ceil(value / 9);
    let superkP = superk * 51.83;
    this.setState({ superkCantidad: superk });
    this.setState({ superkPrecio: superkP });
    let polibreaB = Math.ceil((superk + imperglass) / 8);
    let polibreaBP = polibreaB * 27.5;
    this.setState({ polibreaBCantidad: polibreaB });
    this.setState({ polibreaBPrecio: polibreaBP.toFixed(2) });
    let superacryl = Math.ceil(value / 20);
    let superacrylP = superacryl * 72.5;
    this.setState({ superacrylCantidad: superacryl });
    this.setState({ superacrylPrecio: superacrylP.toFixed(2) });
    let supermalla = Math.ceil(value / 100);
    let supermallaP = supermalla * 164.63;
    this.setState({ supermallaCantidad: supermalla });
    this.setState({ supermallaPrecio: supermallaP.toFixed(2) });
    let antiraiz = Math.ceil(value / 9);
    let antiraizP = antiraiz * 65.85;
    this.setState({ antiraizCantidad: antiraiz });
    this.setState({ antiraizPrecio: antiraizP.toFixed(2) });
    let dren = Math.ceil(value / 40);
    let drenP = dren * 262.2;
    this.setState({ drenCantidad: dren });
    this.setState({ drenPrecio: drenP.toFixed(2) });
    let totalMI = imperglassP + polibreaP + imperlasticP;
    let metroMI = totalMI / value;
    this.setState({ totalMI: totalMI.toFixed(2) });
    this.setState({ metroMI: metroMI.toFixed(2) });
    let totalB = superkP + imperglassP + polibreaBP + imperlasticP;
    let metroB = totalB / value;
    this.setState({ totalB: totalB.toFixed(2) });
    this.setState({ metroB: metroB.toFixed(2) });
    let totalBI = superkP + imperpolP + polibreaBP + imperlasticP;
    let metroBI = totalBI / value;
    this.setState({ totalBI: totalBI.toFixed(2) });
    this.setState({ metroBI: metroBI.toFixed(2) });
    let totalSA = superacrylP + supermallaP;
    let metroSA = totalSA / value;
    this.setState({ totalSA: totalSA.toFixed(2) });
    this.setState({ metroSA: metroSA.toFixed(2) });
    let totalSAO = superacrylP;
    let metroSAO = totalSAO / value;
    this.setState({ totalSAO: totalSAO.toFixed(2) });
    this.setState({ metroSAO: metroSAO.toFixed(2) });
    let totalCA = superkP + drenP + antiraizP + polibreaBP + imperlasticP;
    let metroCA = totalCA / value;
    this.setState({ totalCA: totalCA.toFixed(2) });
    this.setState({ metroCA: metroCA.toFixed(2) });
  }

  render() {
    const { navigation } = this.props;
    const titleName = navigation.getParam("title", "");
    return (
      <ScrollView>
        <ImageBackground
          source={require("../../images/bgImage.png")}
          style={styles.bgProcess}
        >
          <View style={styles.titleContainer}>
            <Text style={styles.textA}>{titleName}</Text>
            <Text style={styles.textB}>
              Ingresa los metros cuadrados del área a cubrir:
            </Text>
          </View>
          <View style={styles.inputContainer}>
            <Text style={styles.textM}>MEDIDAS:</Text>
            <TextInput
              style={styles.dataInput}
              placeholder={"mˆ2"}
              placeholderTextColor={"#858585"}
              blurOnSubmit={true}
              keyboardType={"number-pad"}
              type="number"
              value={this.state.number}
              onChangeText={txt => this.handleChangeNumber("number", txt)}
            />
          </View>
          <View style={styles.resultTitleContainer}>
            <Text style={styles.resultTitle}>{titleName}</Text>
          </View>
          <View style={styles.resultSubtitleContainer}>
            <Text style={styles.results}>Productos</Text>
            <Text style={styles.results}>Cantidad</Text>
            <Text style={styles.results}>Unidad</Text>
            <Text style={styles.results}>Precio</Text>
            <Text style={styles.results}>Rendimiento</Text>
          </View>
          <View style={styles.line} />
          {this.state.monocapaImperpol ? (
            <View>
              <View style={styles.resultSubtitleContainer}>
                <Text style={styles.results}>Imperpol</Text>
                <Text style={styles.results}>
                  {this.state.imperpolCantidad}
                </Text>
                <Text style={styles.results}>Rollo</Text>
                <Text style={styles.results}>{this.state.imperpolPrecio}</Text>
                <Text style={styles.results}>9m2</Text>
              </View>
              <View style={styles.line} />
              <View style={styles.resultSubtitleContainer}>
                <Text style={styles.results}>Polibrea</Text>
                <Text style={styles.results}>
                  {this.state.polibreaCantidad}
                </Text>
                <Text style={styles.results}>CN</Text>
                <Text style={styles.results}>{this.state.polibreaPrecio}</Text>
                <Text style={styles.results}>80m2</Text>
              </View>
              <View style={styles.line} />
              <View style={styles.resultSubtitleContainerF}>
                <Text style={styles.results}>Imperlastic</Text>
                <Text style={styles.results}>
                  {this.state.imperlasticCantidad}
                </Text>
                <Text style={styles.results}>CN</Text>
                <Text style={styles.results}>
                  {this.state.imperlasticPrecio}
                </Text>
                <Text style={styles.results}>100m2</Text>
              </View>
              <View style={styles.totalPrecios}>
                <View style={styles.precioContainer}>
                  <Text style={styles.totalStyle}>Total: </Text>
                  <Text style={styles.metroStyle}>{this.state.totalM}</Text>
                  <Text style={styles.totalStyle}>$</Text>
                </View>
                <View style={styles.precioContainer}>
                  <Text style={styles.totalStyle}>M2: </Text>
                  <Text style={styles.metroStyle}>{this.state.metroM}</Text>
                </View>
              </View>
            </View>
          ) : this.state.monocapaImperglass ? (
            <View>
              <View style={styles.resultSubtitleContainer}>
                <Text style={styles.results}>Imperglass</Text>
                <Text style={styles.results}>
                  {this.state.imperglassCantidad}
                </Text>
                <Text style={styles.results}>Rollo</Text>
                <Text style={styles.results}>
                  {this.state.imperglassPrecio}
                </Text>
                <Text style={styles.results}>9m2</Text>
              </View>
              <View style={styles.line} />
              <View style={styles.resultSubtitleContainer}>
                <Text style={styles.results}>Polibrea</Text>
                <Text style={styles.results}>
                  {this.state.polibreaCantidad}
                </Text>
                <Text style={styles.results}>CN</Text>
                <Text style={styles.results}>{this.state.polibreaPrecio}</Text>
                <Text style={styles.results}>80m2</Text>
              </View>
              <View style={styles.line} />
              <View style={styles.resultSubtitleContainerF}>
                <Text style={styles.results}>Imperlastic</Text>
                <Text style={styles.results}>
                  {this.state.imperlasticCantidad}
                </Text>
                <Text style={styles.results}>CN</Text>
                <Text style={styles.results}>
                  {this.state.imperlasticPrecio}
                </Text>
                <Text style={styles.results}>100m2</Text>
              </View>
              <View style={styles.totalPrecios}>
                <View style={styles.precioContainer}>
                  <Text style={styles.totalStyle}>Total: </Text>
                  <Text style={styles.metroStyle}>{this.state.totalMI}</Text>
                  <Text style={styles.totalStyle}>$</Text>
                </View>
                <View style={styles.precioContainer}>
                  <Text style={styles.totalStyle}>M2: </Text>
                  <Text style={styles.metroStyle}>{this.state.metroMI}</Text>
                </View>
              </View>
            </View>
          ) : this.state.bicapaImperpol ? (
            <View>
              <View style={styles.resultSubtitleContainer}>
                <Text style={styles.results}>SuperK 2500</Text>
                <Text style={styles.results}>{this.state.superkCantidad}</Text>
                <Text style={styles.results}>Rollo</Text>
                <Text style={styles.results}>{this.state.superkPrecio}</Text>
                <Text style={styles.results}>9m2</Text>
              </View>
              <View style={styles.line} />
              <View style={styles.resultSubtitleContainer}>
                <Text style={styles.results}>Imperpol</Text>
                <Text style={styles.results}>
                  {this.state.imperpolCantidad}
                </Text>
                <Text style={styles.results}>Rollo</Text>
                <Text style={styles.results}>{this.state.imperpolPrecio}</Text>
                <Text style={styles.results}>9m2</Text>
              </View>
              <View style={styles.line} />
              <View style={styles.resultSubtitleContainer}>
                <Text style={styles.results}>Polibrea</Text>
                <Text style={styles.results}>
                  {this.state.polibreaBCantidad}
                </Text>
                <Text style={styles.results}>CN</Text>
                <Text style={styles.results}>{this.state.polibreaBPrecio}</Text>
                <Text style={styles.results}>80m2</Text>
              </View>
              <View style={styles.line} />
              <View style={styles.resultSubtitleContainerF}>
                <Text style={styles.results}>Imperlastic</Text>
                <Text style={styles.results}>
                  {this.state.imperlasticCantidad}
                </Text>
                <Text style={styles.results}>CN</Text>
                <Text style={styles.results}>
                  {this.state.imperlasticPrecio}
                </Text>
                <Text style={styles.results}>100m2</Text>
              </View>
              <View style={styles.totalPrecios}>
                <View style={styles.precioContainer}>
                  <Text style={styles.totalStyle}>Total: </Text>
                  <Text style={styles.metroStyle}>{this.state.totalBI}</Text>
                  <Text style={styles.totalStyle}>$</Text>
                </View>
                <View style={styles.precioContainer}>
                  <Text style={styles.totalStyle}>M2: </Text>
                  <Text style={styles.metroStyle}>{this.state.metroBI}</Text>
                </View>
              </View>
            </View>
          ) : this.state.bicapaImperglass ? (
            <View>
              <View style={styles.resultSubtitleContainer}>
                <Text style={styles.results}>SuperK 2500</Text>
                <Text style={styles.results}>{this.state.superkCantidad}</Text>
                <Text style={styles.results}>Rollo</Text>
                <Text style={styles.results}>{this.state.superkPrecio}</Text>
                <Text style={styles.results}>9m2</Text>
              </View>
              <View style={styles.line} />
              <View style={styles.resultSubtitleContainer}>
                <Text style={styles.results}>Imperglass</Text>
                <Text style={styles.results}>
                  {this.state.imperglassCantidad}
                </Text>
                <Text style={styles.results}>Rollo</Text>
                <Text style={styles.results}>
                  {this.state.imperglassPrecio}
                </Text>
                <Text style={styles.results}>9m2</Text>
              </View>
              <View style={styles.line} />
              <View style={styles.resultSubtitleContainer}>
                <Text style={styles.results}>Polibrea</Text>
                <Text style={styles.results}>
                  {this.state.polibreaBCantidad}
                </Text>
                <Text style={styles.results}>CN</Text>
                <Text style={styles.results}>{this.state.polibreaBPrecio}</Text>
                <Text style={styles.results}>80m2</Text>
              </View>
              <View style={styles.line} />
              <View style={styles.resultSubtitleContainerF}>
                <Text style={styles.results}>Imperlastic</Text>
                <Text style={styles.results}>
                  {this.state.imperlasticCantidad}
                </Text>
                <Text style={styles.results}>CN</Text>
                <Text style={styles.results}>
                  {this.state.imperlasticPrecio}
                </Text>
                <Text style={styles.results}>100m2</Text>
              </View>
              <View style={styles.totalPrecios}>
                <View style={styles.precioContainer}>
                  <Text style={styles.totalStyle}>Total: </Text>
                  <Text style={styles.metroStyle}>{this.state.totalB}</Text>
                  <Text style={styles.totalStyle}>$</Text>
                </View>
                <View style={styles.precioContainer}>
                  <Text style={styles.totalStyle}>M2: </Text>
                  <Text style={styles.metroStyle}>{this.state.metroB}</Text>
                </View>
              </View>
            </View>
          ) : this.state.superacryl ? (
            <ScrollView style={styles.hozView} horizontal={true}>
              <View>
                <View style={styles.resultSubtitleContainer}>
                  <Text style={styles.results}>Superacryl</Text>
                  <Text style={styles.results}>
                    {this.state.superacrylCantidad}
                  </Text>
                  <Text style={styles.results}>Rollo</Text>
                  <Text style={styles.results}>
                    {this.state.superacrylPrecio}
                  </Text>
                  <Text style={styles.results}>20m2</Text>
                </View>
                <View style={styles.line} />
                <View style={styles.resultSubtitleContainerF}>
                  <Text style={styles.results}>Supermalla</Text>
                  <Text style={styles.results}>
                    {this.state.supermallaCantidad}
                  </Text>
                  <Text style={styles.results}>CN</Text>
                  <Text style={styles.results}>
                    {this.state.supermallaPrecio}
                  </Text>
                  <Text style={styles.results}>100m2</Text>
                </View>
                <View style={styles.totalPrecios}>
                  <View style={styles.precioContainer}>
                    <Text style={styles.totalStyle}>Total: </Text>
                    <Text style={styles.metroStyle}>{this.state.totalSA}</Text>
                    <Text style={styles.totalStyle}>$</Text>
                  </View>
                  <View style={styles.precioContainer}>
                    <Text style={styles.totalStyle}>M2: </Text>
                    <Text style={styles.metroStyle}>{this.state.metroSA}</Text>
                  </View>
                </View>
              </View>
              <View>
                <View style={styles.resultSubtitleContainerF}>
                  <Text style={styles.results}>Superacryl</Text>
                  <Text style={styles.results}>
                    {this.state.superacrylCantidad}
                  </Text>
                  <Text style={styles.results}>Rollo</Text>
                  <Text style={styles.results}>
                    {this.state.superacrylPrecio}
                  </Text>
                  <Text style={styles.results}>20m2</Text>
                </View>
                <View style={styles.line} />
                <View style={styles.totalPrecios}>
                  <View style={styles.precioContainer}>
                    <Text style={styles.totalStyle}>Total: </Text>
                    <Text style={styles.metroStyle}>{this.state.totalSAO}</Text>
                    <Text style={styles.totalStyle}>$</Text>
                  </View>
                  <View style={styles.precioContainer}>
                    <Text style={styles.totalStyle}>M2: </Text>
                    <Text style={styles.metroStyle}>{this.state.metroSAO}</Text>
                  </View>
                </View>
              </View>
            </ScrollView>
          ) : this.state.cubiertas ? (
            <View>
              <View style={styles.resultSubtitleContainer}>
                <Text style={styles.results}>SuperK 2500</Text>
                <Text style={styles.results}>{this.state.superkCantidad}</Text>
                <Text style={styles.results}>Rollo</Text>
                <Text style={styles.results}>{this.state.superkPrecio}</Text>
                <Text style={styles.results}>9m2</Text>
              </View>
              <View style={styles.line} />
              <View style={styles.resultSubtitleContainer}>
                <Text style={styles.results}>Antiraiz</Text>
                <Text style={styles.results}>
                  {this.state.antiraizCantidad}
                </Text>
                <Text style={styles.results}>Rollo</Text>
                <Text style={styles.results}>{this.state.antiraizPrecio}</Text>
                <Text style={styles.results}>9m2</Text>
              </View>
              <View style={styles.line} />
              <View style={styles.resultSubtitleContainer}>
                <Text style={styles.results}>Dren</Text>
                <Text style={styles.results}>{this.state.drenCantidad}</Text>
                <Text style={styles.results}>Rollo</Text>
                <Text style={styles.results}>{this.state.drenPrecio}</Text>
                <Text style={styles.results}>40m2</Text>
              </View>
              <View style={styles.line} />
              <View style={styles.resultSubtitleContainer}>
                <Text style={styles.results}>Polibrea</Text>
                <Text style={styles.results}>
                  {this.state.polibreaBCantidad}
                </Text>
                <Text style={styles.results}>CN</Text>
                <Text style={styles.results}>{this.state.polibreaBPrecio}</Text>
                <Text style={styles.results}>80m2</Text>
              </View>
              <View style={styles.line} />
              <View style={styles.resultSubtitleContainerF}>
                <Text style={styles.results}>Imperlastic</Text>
                <Text style={styles.results}>
                  {this.state.imperlasticCantidad}
                </Text>
                <Text style={styles.results}>CN</Text>
                <Text style={styles.results}>
                  {this.state.imperlasticPrecio}
                </Text>
                <Text style={styles.results}>100m2</Text>
              </View>
              <View style={styles.totalPrecios}>
                <View style={styles.precioContainer}>
                  <Text style={styles.totalStyle}>Total: </Text>
                  <Text style={styles.metroStyle}>{this.state.totalCA}</Text>
                  <Text style={styles.totalStyle}>$</Text>
                </View>
                <View style={styles.precioContainer}>
                  <Text style={styles.totalStyle}>M2: </Text>
                  <Text style={styles.metroStyle}>{this.state.metroCA}</Text>
                </View>
              </View>
            </View>
          ) : null}
          <View style={styles.btnContainer}>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate("Contacto")}
            >
              <Image
                style={styles.btnFind}
                source={require("../../images/find.png")}
              />
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() =>
                Linking.openURL(
                  "https://api.whatsapp.com/send?phone=593987700909"
                )
              }
            >
              <Image
                style={styles.btnAsesor}
                source={require("../../images/asesor.png")}
              />
            </TouchableOpacity>
          </View>
        </ImageBackground>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  titleContainer: {
    marginTop: responsiveHeight(4),
    width: responsiveWidth(90),
    marginLeft: responsiveWidth(5)
  },
  textA: {
    fontSize: responsiveFontSize(4),
    color: "white"
  },
  textB: {
    marginTop: responsiveHeight(2),
    fontSize: responsiveFontSize(2),
    color: "white"
  },
  bgProcess: {
    width: responsiveWidth(100),
    height: responsiveHeight(100)
  },
  inputContainer: {
    width: responsiveWidth(90),
    marginLeft: responsiveWidth(5),
    marginTop: responsiveHeight(3),
    backgroundColor: "white",
    borderRadius: 10
  },
  textM: {
    fontSize: responsiveFontSize(4),
    marginLeft: responsiveWidth(5),
    marginTop: responsiveHeight(2)
  },
  dataInput: {
    width: responsiveWidth(80),
    marginTop: responsiveHeight(4),
    backgroundColor: "transparent",
    borderBottomWidth: responsiveWidth(0.1),
    borderBottomColor: "black",
    marginBottom: responsiveHeight(5),
    marginLeft: responsiveWidth(5),
    textAlign: "center"
  },
  resultTitleContainer: {
    width: responsiveWidth(90),
    marginLeft: responsiveWidth(5),
    backgroundColor: "#cddb00",
    marginTop: responsiveHeight(5)
  },
  resultTitle: {
    color: "white",
    fontSize: responsiveFontSize(3),
    marginLeft: responsiveWidth(5),
    fontWeight: "bold",
    marginTop: responsiveHeight(2),
    marginBottom: responsiveHeight(2)
  },
  resultSubtitleContainer: {
    flexDirection: "row",
    backgroundColor: "white",
    width: responsiveWidth(90),
    marginLeft: responsiveWidth(5)
  },
  resultSubtitleContainerF: {
    flexDirection: "row",
    backgroundColor: "white",
    width: responsiveWidth(90),
    marginLeft: responsiveWidth(5),
    borderBottomRightRadius: 7,
    borderBottomLeftRadius: 7
  },
  results: {
    marginTop: responsiveHeight(1),
    marginBottom: responsiveHeight(1),
    width: responsiveWidth(18),
    textAlign: "center",
    color: "#4d4d4d",
    fontSize: responsiveFontSize(1.4)
  },
  line: {
    borderBottomColor: "#808080",
    borderBottomWidth: 1,
    width: responsiveWidth(90),
    marginLeft: responsiveWidth(5)
  },
  btnContainer: {
    marginTop: responsiveHeight(3),
    flexDirection: "row"
  },
  btnFind: {
    resizeMode: "contain",
    width: responsiveWidth(40),
    height: Math.round((responsiveWidth(40) * 1) / 3.39),
    marginLeft: responsiveWidth(5)
  },
  btnAsesor: {
    resizeMode: "contain",
    width: responsiveWidth(40),
    height: Math.round((responsiveWidth(40) * 1) / 3.39),
    marginLeft: responsiveWidth(10)
  },
  totalPrecios: {
    marginTop: responsiveHeight(2),
    width: responsiveWidth(40),
    marginLeft: responsiveWidth(20)
  },
  precioContainer: {
    flexDirection: "row"
  },
  totalStyle: {
    color: "white",
    fontSize: responsiveFontSize(3),
    fontWeight: "bold"
  },
  metroStyle: {
    color: "white",
    fontSize: responsiveFontSize(3),
    marginLeft: responsiveWidth(5)
  },
  hozView: {
    marginRight: responsiveWidth(5)
  }
});
