import React, { Component } from "react";
import {
  ScrollView,
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity
} from "react-native";
import {
  responsiveFontSize,
  responsiveHeight,
  responsiveWidth
} from "react-native-responsive-dimensions";
import { Overlay } from "react-native-elements";

export default class ImperScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isVisible: false,
      isVisibleBi: false,
      isVisibleSu: false
    };
    this.openOverlay = this.openOverlay.bind(this);
    this.openOverlayBi = this.openOverlayBi.bind(this);
    this.openOverlaySu = this.openOverlaySu.bind(this);
    this.openMonocapaImperpol = this.openMonocapaImperpol.bind(this);
    this.openMonocapaImperglass = this.openMonocapaImperglass.bind(this);
    this.openBicapaImperpol = this.openBicapaImperpol.bind(this);
    this.openBicapaImperglass = this.openBicapaImperglass.bind(this);
    this.openSuperacryl = this.openSuperacryl.bind(this);
    this.openSuperacrylMalla = this.openSuperacrylMalla.bind(this);
  }
  static navigationOptions = {
    title: "Impermeabilización total",
    headerStyle: {
      backgroundColor: "#20A5DC"
    },
    headerTitleStyle: {
      color: "white"
    }
  };
  openOverlay() {
    this.setState({ isVisible: true });
  }
  openMonocapaImperpol() {
    this.props.navigation.navigate("Process", {
      title: "Sistemas monocapa Imperpol"
    });
    this.setState({ isVisible: false });
  }
  openMonocapaImperglass() {
    this.props.navigation.navigate("Process", {
      title: "Sistemas monocapa Imperglass"
    });
    this.setState({ isVisible: false });
  }
  openOverlayBi() {
    this.setState({ isVisibleBi: true });
  }
  openBicapaImperpol() {
    this.props.navigation.navigate("Process", {
      title: "Sistemas bicapa Imperpol"
    });
    this.setState({ isVisibleBi: false });
  }
  openBicapaImperglass() {
    this.props.navigation.navigate("Process", {
      title: "Sistemas bicapa Imperglass"
    });
    this.setState({ isVisibleBi: false });
  }
  openOverlaySu() {
    this.setState({ isVisibleSu: true });
  }
  openSuperacryl() {
    this.props.navigation.navigate("Process", {
      title: "SUPERACRYL"
    });
    this.setState({ isVisibleSu: false });
  }
  openSuperacrylMalla() {
    this.props.navigation.navigate("Process", {
      title: "SUPERACRYL MALLA"
    });
    this.setState({ isVisibleSu: false });
  }
  render() {
    return (
      <ScrollView style={styles.bgImper}>
        <Overlay
          isVisible={this.state.isVisible}
          borderRadius={5}
          windowBackgroundColor="rgba(0, 0, 0, .3)"
          overlayBackgroundColor="#f1f1f1"
          width={responsiveWidth(90)}
          height="auto"
          onBackdropPress={() => this.setState({ isVisible: false })}
        >
          <Text style={styles.textOverlay}>
            Elige la opción a impermeabilizar:
          </Text>
          <TouchableOpacity onPress={this.openMonocapaImperpol}>
            <Image
              style={styles.buttonOverlay}
              source={require("../../images/usa-imperpol.png")}
            />
          </TouchableOpacity>
          <TouchableOpacity onPress={this.openMonocapaImperglass}>
            <Image
              style={styles.buttonOverlay}
              source={require("../../images/usa-imperglass.png")}
            />
          </TouchableOpacity>
        </Overlay>
        <Overlay
          isVisible={this.state.isVisibleBi}
          borderRadius={5}
          windowBackgroundColor="rgba(0, 0, 0, .3)"
          overlayBackgroundColor="#f1f1f1"
          width={responsiveWidth(90)}
          height="auto"
          onBackdropPress={() => this.setState({ isVisibleBi: false })}
        >
          <Text style={styles.textOverlay}>
            Elige la opción a impermeabilizar:
          </Text>
          <TouchableOpacity onPress={this.openBicapaImperpol}>
            <Image
              style={styles.buttonOverlay}
              source={require("../../images/usa-imperpol.png")}
            />
          </TouchableOpacity>
          <TouchableOpacity onPress={this.openBicapaImperglass}>
            <Image
              style={styles.buttonOverlay}
              source={require("../../images/usa-imperglass.png")}
            />
          </TouchableOpacity>
        </Overlay>
        <Overlay
          isVisible={this.state.isVisibleSu}
          borderRadius={5}
          windowBackgroundColor="rgba(0, 0, 0, .3)"
          overlayBackgroundColor="#f1f1f1"
          width={responsiveWidth(90)}
          height="auto"
          onBackdropPress={() => this.setState({ isVisibleSu: false })}
        >
          <Text style={styles.textOverlay}>Elige la opción de superacryl:</Text>
          <TouchableOpacity onPress={this.openSuperacryl}>
            <Image
              style={styles.buttonOverlay}
              source={require("../../images/superacryl-only.png")}
            />
          </TouchableOpacity>
          <TouchableOpacity onPress={this.openSuperacrylMalla}>
            <Image
              style={styles.buttonOverlay}
              source={require("../../images/supermalla.png")}
            />
          </TouchableOpacity>
        </Overlay>
        <View style={styles.textAContainer}>
          <Text style={styles.textA}>Cotiza tu</Text>
          <Text style={styles.textA}>solución de </Text>
          <Text style={styles.textA}>impermeabilización</Text>
        </View>
        <View style={styles.textBContainer}>
          <Text style={styles.textB}>Tenemos las soluciones más</Text>
          <Text style={styles.textB}>eficientes para ti.</Text>
        </View>
        <TouchableOpacity onPress={this.openOverlay}>
          <Image
            style={styles.buttonOp}
            source={require("../../images/monocapa.png")}
          />
        </TouchableOpacity>
        <TouchableOpacity onPress={this.openOverlayBi}>
          <Image
            style={styles.buttonOp}
            source={require("../../images/bicapa.png")}
          />
        </TouchableOpacity>
        <TouchableOpacity onPress={this.openOverlaySu}>
          <Image
            style={styles.buttonOp}
            source={require("../../images/superacrylBtn.png")}
          />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() =>
            this.props.navigation.navigate("Process", {
              title: "Cubiertas ajardinadas"
            })
          }
        >
          <Image
            style={styles.buttonOpFinal}
            source={require("../../images/cubiertas.png")}
          />
        </TouchableOpacity>
      </ScrollView>
    );
  }
}
const styles = StyleSheet.create({
  bgImper: {
    backgroundColor: "#f1f1f1"
  },
  textAContainer: {
    marginTop: responsiveHeight(3),
    width: responsiveWidth(90),
    marginLeft: responsiveWidth(5)
  },
  textA: {
    fontSize: responsiveFontSize(4),
    color: "#4d4d4d"
  },
  textBContainer: {
    width: responsiveWidth(90),
    marginLeft: responsiveWidth(5)
  },
  textB: {
    fontSize: responsiveFontSize(2),
    color: "#4d4d4d"
  },
  buttonOp: {
    marginTop: responsiveHeight(3),
    resizeMode: "contain",
    width: responsiveWidth(90),
    height: Math.round((responsiveWidth(90) * 1) / 3.51),
    marginLeft: responsiveWidth(5)
  },
  buttonOpFinal: {
    marginTop: responsiveHeight(3),
    resizeMode: "contain",
    width: responsiveWidth(90),
    height: Math.round((responsiveWidth(90) * 1) / 3.51),
    marginLeft: responsiveWidth(5),
    marginBottom: responsiveHeight(3)
  },
  buttonOverlay: {
    marginTop: responsiveHeight(4),
    resizeMode: "contain",
    width: responsiveWidth(80),
    height: Math.round((responsiveWidth(80) * 1) / 3.51),
    marginLeft: responsiveWidth(2.5)
  },
  textOverlay: {
    fontSize: responsiveFontSize(3),
    marginLeft: responsiveWidth(5)
  }
});
