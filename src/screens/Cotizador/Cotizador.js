import React, { Component } from "react";
import {
  Button,
  Text,
  View,
  ScrollView,
  ImageBackground,
  StyleSheet,
  Image,
  TouchableOpacity
} from "react-native";
import {
  responsiveHeight,
  responsiveWidth,
  responsiveFontSize
} from "react-native-responsive-dimensions";

export default class CotizadorScreen extends React.Component {
  static navigationOptions = {
    title: "Cotizador",
    headerLeft: null,
    headerStyle: {
      backgroundColor: "#20A5DC"
    },
    headerTitleStyle: {
      color: "white"
    }
  };

  render() {
    return (
      <ScrollView>
        <ImageBackground
          source={require("../../images/bgImage.png")}
          style={styles.bgCotImage}
        >
          <View style={styles.textAContainer}>
            <Text style={styles.textA}>¿Necesitas</Text>
            <Text style={styles.textA}>impermeabilizar?</Text>
          </View>
          <View style={styles.textBContainer}>
            <Text style={styles.textB}>Tenemos las soluciones más</Text>
            <Text style={styles.textB}>eficientes para ti.</Text>
          </View>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate("Imper")}
          >
            <Image
              style={styles.imper}
              source={require("../../images/imper.png")}
            />
          </TouchableOpacity>
          {/*<TouchableOpacity>*/}
          {/*  <Image*/}
          {/*    style={styles.imper}*/}
          {/*    source={require("../../images/arqui.png")}*/}
          {/*  />*/}
          {/*</TouchableOpacity>*/}
          {/*<TouchableOpacity>*/}
          {/*  <Image*/}
          {/*    style={styles.imper}*/}
          {/*    source={require("../../images/vial.png")}*/}
          {/*  />*/}
          {/*</TouchableOpacity>*/}
        </ImageBackground>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  bgCotImage: {
    width: responsiveWidth(100),
    height: responsiveHeight(100)
  },
  imper: {
    marginTop: responsiveHeight(3),
    resizeMode: "contain",
    width: responsiveWidth(90),
    height: Math.round((responsiveWidth(90) * 1) / 4.19),
    marginLeft: responsiveWidth(5)
  },
  textAContainer: {
    marginTop: responsiveHeight(8),
    marginLeft: responsiveWidth(5),
    width: responsiveWidth(90)
  },
  textA: {
    fontSize: responsiveFontSize(4),
    color: "white",
    fontWeight: "bold"
  },
  textBContainer: {
    width: responsiveWidth(90),
    marginLeft: responsiveWidth(5)
  },
  textB: {
    fontSize: responsiveFontSize(3),
    color: "white"
  }
});
