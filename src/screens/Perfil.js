import React, { Component } from "react";
import {
  Button,
  Text,
  View,
  ScrollView,
  StyleSheet,
  ImageBackground
} from "react-native";
import * as firebase from "firebase";
import { Avatar } from "react-native-elements";
import {
  responsiveFontSize,
  responsiveHeight,
  responsiveWidth
} from "react-native-responsive-dimensions";
import Ionicons from "react-native-vector-icons/Ionicons";

export default class PerfilScreen extends React.Component {
  static navigationOptions = {
    title: "Mi cuenta",
    headerLeft: null,
    headerStyle: {
      backgroundColor: "#20A5DC"
    },
    headerTitleStyle: {
      color: "white"
    }
  };

  onLogout() {
    firebase
      .auth()
      .signOut()
      .then(function() {
        // Sign-out successful.
      })
      .catch(function(error) {
        // An error happened.
      });
  }
  render() {
    return (
      <ScrollView style={styles.bgPerfil}>
        <ImageBackground
          source={require("../images/bgPerfil.png")}
          style={styles.bgPerfilImg}
        >
          <View style={styles.bgUp}>
            <Avatar
              size="xlarge"
              rounded
              source={{
                uri:
                  "https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg"
              }}
            />
            <Text style={styles.userName}>firstname lastname</Text>
          </View>
        </ImageBackground>
        <Text style={styles.titlePerfil}>Mi información</Text>
        <View style={styles.iconText}>
          <Ionicons name={"md-person"} size={25} color={"#989898"} />
          <Text style={styles.textInfo}>Cuenta</Text>
        </View>
        <View style={styles.iconText}>
          <Ionicons
            name={"md-information-circle-outline"}
            size={25}
            color={"#989898"}
          />
          <Text style={styles.textInfo}>Información</Text>
        </View>
        <View style={styles.line} />
        <Text style={styles.titlePerfil}>Historial</Text>
        <View style={styles.iconText}>
          <Ionicons name={"md-document"} size={25} color={"#989898"} />
          <Text style={styles.textInfo}>Cotizaciones</Text>
        </View>
        <View style={styles.line} />
        <Text style={styles.titlePerfil}>App</Text>
        <View style={styles.iconText}>
          <Ionicons
            name={"md-notifications-outline"}
            size={25}
            color={"#989898"}
          />
          <Text style={styles.textInfo}>Notificaciones</Text>
        </View>
        <View style={styles.iconText}>
          <Ionicons name={"md-star-outline"} size={25} color={"#989898"} />
          <Text style={styles.textInfo}>Valóranos</Text>
        </View>
        <View style={styles.iconText}>
          <Ionicons name={"md-help"} size={25} color={"#989898"} />
          <Text style={styles.textInfo}>Preguntas Frecuentes</Text>
        </View>
        <View style={styles.iconTextF}>
          <Ionicons name={"md-exit"} size={25} color={"#989898"} />
          <Text onPress={this.onLogout} style={styles.textInfo}>
            Cerrar sesión
          </Text>
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  bgPerfilImg: {
    width: responsiveWidth(100),
    height: responsiveHeight(35)
  },
  bgUp: {
    width: responsiveWidth(100),
    height: responsiveHeight(35),
    justifyContent: "center",
    alignItems: "center"
  },
  titlePerfil: {
    color: "#666666",
    fontSize: responsiveFontSize(2),
    marginLeft: responsiveWidth(5),
    marginTop: responsiveHeight(2)
  },
  userName: {
    color: "black",
    fontSize: responsiveFontSize(4)
  },
  iconText: {
    marginTop: responsiveHeight(1),
    width: responsiveWidth(90),
    marginLeft: responsiveWidth(5),
    flexDirection: "row",
    alignItems: "center"
  },
  iconTextF: {
    marginTop: responsiveHeight(1),
    width: responsiveWidth(90),
    marginLeft: responsiveWidth(5),
    flexDirection: "row",
    alignItems: "center",
    marginBottom: responsiveHeight(2)
  },
  textInfo: {
    fontSize: responsiveFontSize(2),
    color: "black",
    marginLeft: responsiveWidth(2)
  },
  line: {
    marginTop: responsiveHeight(1),
    borderBottomColor: "#f1f1f1",
    borderBottomWidth: 9,
    width: responsiveWidth(100)
  }
});
