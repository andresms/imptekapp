import React from "react";
import {
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  SafeAreaView,
  Alert
} from "react-native";
import {
  responsiveFontSize,
  responsiveHeight,
  responsiveWidth
} from "react-native-responsive-dimensions";
import CardView from "react-native-cardview";
import { Card, ListItem, Button, Icon } from "react-native-elements";
import * as firebase from "firebase";

const tecnicos = [
  {
    avatar:
      "https://s3.amazonaws.com/cms-dev.imptek.com/promo/tecnico/tecnicos3Artboard+1+copy.png"
  },
  {
    avatar:
      "https://s3.amazonaws.com/cms-dev.imptek.com/promo/tecnico/tecnicos3Artboard+1+copy+6.png"
  },
  {
    avatar:
      "https://s3.amazonaws.com/cms-dev.imptek.com/promo/tecnico/tecnicos3Artboard+1+copy+5.png"
  },
  {
    avatar:
      "https://s3.amazonaws.com/cms-dev.imptek.com/promo/tecnico/tecnicos3Artboard+1+copy+4.png"
  },
  {
    avatar:
      "https://s3.amazonaws.com/cms-dev.imptek.com/promo/tecnico/tecnicos4Artboard+1+copy+2.png"
  },
  {
    avatar:
      "https://s3.amazonaws.com/cms-dev.imptek.com/promo/tecnico/tecnicos4Artboard+1+copy+7.png"
  },
  {
    avatar:
      "https://s3.amazonaws.com/cms-dev.imptek.com/promo/tecnico/tecnicos4Artboard+1+copy+8.png"
  },
  {
    avatar:
      "https://s3.amazonaws.com/cms-dev.imptek.com/promo/tecnico/tecnicos4Artboard+1+copy+9.png"
  },
  {
    avatar:
      "https://s3.amazonaws.com/cms-dev.imptek.com/promo/tecnico/tecnicos4Artboard+1+copy+10.png"
  },
  {
    avatar:
      "https://s3.amazonaws.com/cms-dev.imptek.com/promo/tecnico/tecnicos4Artboard+1+copy+11.png"
  },
  {
    avatar: "https://s3.amazonaws.com/cms-dev.imptek.com/promo/tecnico/1.png"
  },
  {
    avatar: "https://s3.amazonaws.com/cms-dev.imptek.com/promo/tecnico/2.png"
  },
  {
    avatar: "https://s3.amazonaws.com/cms-dev.imptek.com/promo/tecnico/3.png"
  },
  {
    avatar: "https://s3.amazonaws.com/cms-dev.imptek.com/promo/tecnico/4.png"
  }
];

const nacional = [
  {
    avatar: "https://s3.amazonaws.com/cms-dev.imptek.com/promo/nacional/1.png"
  },
  {
    avatar: "https://s3.amazonaws.com/cms-dev.imptek.com/promo/nacional/2.png"
  },
  {
    avatar: "https://s3.amazonaws.com/cms-dev.imptek.com/promo/nacional/3.png"
  },
  {
    avatar: "https://s3.amazonaws.com/cms-dev.imptek.com/promo/nacional/4.png"
  },
  {
    avatar: "https://s3.amazonaws.com/cms-dev.imptek.com/promo/nacional/5.png"
  }
];

const expor = [
  {
    avatar: "https://s3.amazonaws.com/cms-dev.imptek.com/promo/exportador/1.png"
  },
  {
    avatar: "https://s3.amazonaws.com/cms-dev.imptek.com/promo/exportador/2.png"
  },
  {
    avatar: "https://s3.amazonaws.com/cms-dev.imptek.com/promo/exportador/3.png"
  }
];

export default class PromocionesScreen extends React.Component {
  static navigationOptions = {
    title: "Promociones y ofertas",
    headerStyle: {
      backgroundColor: "#20A5DC"
    },
    headerTitleStyle: {
      color: "white"
    }
  };

  constructor(props) {
    super(props);
    this.state = {
      roleNumber: 0
    };
  }

  componentDidMount() {
    try {
      let email = firebase.auth().currentUser.email;
      firebase
        .database()
        .ref("users")
        .orderByChild("email")
        .equalTo(email)
        .once("value", snapshot => {
          snapshot.forEach(data => {
            const role = data.val().role;
            this.setState({
              roleNumber: role
            });
          });
        });
    } catch (e) {
      Alert.alert(e.toString());
    }
  }

  render() {
    if (this.state.roleNumber === 1) {
      return (
        <ScrollView>
          <Card>
            {tecnicos.map((u, i) => {
              return (
                <View key={i} style={styles.listContainer}>
                  <Image style={styles.image} source={{ uri: u.avatar }} />
                </View>
              );
            })}
          </Card>
        </ScrollView>
      );
    }
    if (this.state.roleNumber === 2) {
      return (
        <ScrollView>
          <Card>
            {nacional.map((u, i) => {
              return (
                <View key={i} style={styles.listContainer}>
                  <Image style={styles.image} source={{ uri: u.avatar }} />
                </View>
              );
            })}
          </Card>
        </ScrollView>
      );
    }

    if (this.state.roleNumber === 3) {
      return (
        <ScrollView>
          <Card>
            {expor.map((u, i) => {
              return (
                <View key={i} style={styles.listContainer}>
                  <Image style={styles.image} source={{ uri: u.avatar }} />
                </View>
              );
            })}
          </Card>
        </ScrollView>
      );
    }

    return null;
  }
}
const styles = StyleSheet.create({
  totalContainer: {
    width: responsiveWidth(100),
    alignItems: "center"
  },
  image: {
    width: responsiveWidth(90),
    height: Math.round((responsiveWidth(90) * 1) / 1.61),
    marginRight: responsiveWidth(17),
    borderRadius: 10,
    marginTop: responsiveHeight(2)
  },
  listContainer: {
    width: responsiveWidth(100),
    alignItems: "center"
  },
  fullContainer: {
    backgroundColor: "#9C9B9B",
    width: responsiveWidth(100),
    height: responsiveHeight(100),
    alignItems: "center"
  },
  cardContainer1: {
    width: responsiveWidth(90),
    height: Math.round((responsiveWidth(90) * 1) / 2.42),
    marginLeft: "auto",
    marginRight: "auto",
    zIndex: 0,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 12
    },
    shadowOpacity: 0.58,
    shadowRadius: 16.0,
    elevation: 20,
    marginBottom: responsiveHeight(3)
  },
  cardContainerA: {
    width: responsiveWidth(90),
    height: Math.round((responsiveWidth(90) * 1) / 2.42),
    marginLeft: "auto",
    marginRight: "auto",
    zIndex: 0,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 12
    },
    shadowOpacity: 0.58,
    shadowRadius: 16.0,
    elevation: 20,
    marginBottom: responsiveHeight(3),
    marginTop: responsiveHeight(5)
  },
  card1: {
    resizeMode: "contain",
    width: responsiveWidth(89),
    height: Math.round((responsiveWidth(90) * 1) / 2.42)
  },
  cardContainerD: {
    width: responsiveWidth(90),
    height: Math.round((responsiveWidth(90) * 1) / 2.42),
    marginLeft: "auto",
    marginRight: "auto",
    zIndex: 0,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 12
    },
    shadowOpacity: 0.58,
    shadowRadius: 16.0,
    elevation: 20,
    marginBottom: responsiveHeight(3)
  },
  cardD: {
    resizeMode: "contain",
    width: responsiveWidth(90),
    height: Math.round((responsiveWidth(90) * 1) / 2.42)
  }
});
