import React from "react";
import {
  View,
  Text,
  Image,
  Animated,
  StyleSheet,
  ImageBackground,
  TouchableOpacity,
  Alert,
  Linking,
  ScrollView
} from "react-native";
import {
  responsiveHeight,
  responsiveWidth,
  responsiveFontSize
} from "react-native-responsive-dimensions";
import * as firebase from "firebase";

export default class HomeExpoScreen extends React.Component {
  static navigationOptions = {
    title: "Novena Convención",
    headerStyle: {
      backgroundColor: "#20A5DC"
    },
    headerTitleStyle: {
      color: "white"
    }
  };
  constructor(props) {
    super(props);
    this.state = {
      accessToken: "",
      roleNumber: 0,
      title: "Default",
      body: "Default"
    };
  }

  componentDidMount() {
    try {
      let email = firebase.auth().currentUser.email;
      firebase
        .database()
        .ref("users")
        .orderByChild("email")
        .equalTo(email)
        .once("value", snapshot => {
          snapshot.forEach(data => {
            const role = data.val().role;
            this.setState({
              roleNumber: role
            });
          });
        });
    } catch (e) {
      Alert.alert(e.toString());
    }
  }
  onLogout() {
    firebase.auth().signOut();
    this.props.navigation.push("Intro");
  }

  render() {
    if (this.state.roleNumber >= 0) {
      return (
        <ScrollView style={styles.parentcontainer}>
          {/*<Text style={styles.text}>¿Qué deseas{"\n"}hacer hoy?</Text>*/}
          {/*<View style={styles.line} />*/}
          {/*<Text style={styles.textA}>actividades</Text>*/}

          <View
            style={{ flexDirection: "row", marginTop: responsiveHeight(3) }}
          >
            <View
              style={{
                flexDirection: "column",
                marginRight: "auto",
                marginLeft: responsiveWidth(5),
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <View style={styles.cardMemoriesA}>
                <TouchableOpacity
                  onPress={() => this.props.navigation.navigate("Memories")}
                >
                  <Image
                    style={styles.cardM}
                    source={require("../../images/memorias_home.png")}
                  />
                </TouchableOpacity>
              </View>

              <Text style={styles.textHomeExpo}>Te contamos más</Text>
              <Text style={styles.textHomeExpo2}>Mira más del{"\n"}evento</Text>
              <View style={styles.lineTwo} />

              <View style={styles.cardMemories}>
                <TouchableOpacity
                  onPress={() =>
                    this.props.navigation.push("MaterialDescargable")
                  }
                >
                  <Image
                    style={styles.cardC}
                    source={require("../../images/material_home.png")}
                  />
                </TouchableOpacity>
              </View>
              <View style={styles.cardMemories}>
                <TouchableOpacity
                  onPress={() => this.props.navigation.push("Promociones")}
                >
                  <Image
                    style={styles.cardC}
                    source={require("../../images/promociones_home.png")}
                  />
                </TouchableOpacity>
              </View>
              <View style={styles.cardVideo}>
                <TouchableOpacity
                  onPress={() =>
                    this.props.navigation.push("Video", {
                      url:
                        "https://s3.amazonaws.com/cms-dev.imptek.com/memories/video.mp4"
                    })
                  }
                >
                  <Image
                    style={styles.cardV}
                    source={require("../../images/video.png")}
                  />
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </ScrollView>
      );
    } else {
      return (
        <ScrollView style={styles.parentcontainer}>
          <Text style={styles.text}>¿Qué deseas{"\n"}hacer hoy?</Text>
          <View style={styles.line} />
          <Text style={styles.textA}>actividades</Text>

          <View
            style={{ flexDirection: "row", marginTop: responsiveHeight(3) }}
          >
            <View
              style={{
                flexDirection: "column",
                marginRight: "auto",
                marginLeft: responsiveWidth(5),
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <View style={styles.cardMemoriesA}>
                <TouchableOpacity
                  onPress={() => this.props.navigation.navigate("Memories")}
                >
                  <Image
                    style={styles.cardM}
                    source={require("../../images/memorias_home.png")}
                  />
                </TouchableOpacity>
              </View>

              <Text style={styles.textHomeExpo}>Te contamos más</Text>
              <Text style={styles.textHomeExpo2}>Mira más del{"\n"}evento</Text>
              <View style={styles.lineTwo} />

              <View style={styles.cardVideo}>
                <TouchableOpacity
                  onPress={() => this.props.navigation.push("Video")}
                >
                  <Image
                    style={styles.cardV}
                    source={require("../../images/video.png")}
                  />
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </ScrollView>
      );
    }
  }
}

const styles = StyleSheet.create({
  parentcontainer: {
    flex: 1,
    backgroundColor: "#E4E4E4",
    width: responsiveWidth(100)
  },
  logo: {
    left: responsiveWidth(20),
    width: responsiveWidth(25),
    height: Math.round((responsiveWidth(25) * 1) / 2.3),
    resizeMode: "contain",
    zIndex: 5,
    marginTop: responsiveHeight(5),
    elevation: 25
  },
  text: {
    marginTop: responsiveHeight(5),
    fontSize: responsiveFontSize(3),
    color: "#666666",
    marginLeft: responsiveWidth(5),
    textAlign: "left"
  },
  textHomeExpo: {
    marginTop: responsiveHeight(2),
    fontSize: responsiveFontSize(2),
    color: "#666666",
    marginLeft: responsiveWidth(5),
    textAlign: "left",
    marginRight: "auto"
  },
  textHomeExpo2: {
    fontSize: responsiveFontSize(4),
    color: "#666666",
    marginLeft: responsiveWidth(5),
    textAlign: "left",
    marginRight: "auto"
  },
  line: {
    borderBottomColor: "#666666",
    borderBottomWidth: 1,
    width: responsiveWidth(80),
    marginLeft: responsiveWidth(-15)
  },
  lineTwo: {
    borderBottomColor: "black",
    borderBottomWidth: 1,
    width: responsiveWidth(80),
    marginLeft: responsiveWidth(-15),
    marginBottom: responsiveHeight(2)
  },
  textA: {
    marginLeft: responsiveWidth(5),
    color: "#666666",
    fontSize: responsiveFontSize(2)
  },
  cardContainer: {
    flex: 1,
    alignItems: "center"
  },
  cardEvents: {
    width: responsiveWidth(42.5),
    height: Math.round((responsiveWidth(42.5) * 1) / 0.81),
    marginLeft: responsiveWidth(5),
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 12
    },
    shadowOpacity: 0.58,
    shadowRadius: 16.0,
    elevation: 20
  },
  cardPromo: {
    marginLeft: responsiveWidth(5),
    width: responsiveWidth(42.5),
    height: Math.round((responsiveWidth(45) * 1) / 1.78),
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 12
    },
    shadowOpacity: 0.58,
    shadowRadius: 16.0,
    elevation: 20
  },
  cardE: {
    resizeMode: "contain",
    width: responsiveWidth(42.5),
    height: Math.round((responsiveWidth(42.5) * 1) / 0.81),
    alignItems: "center"
  },
  cardMemories: {
    width: responsiveWidth(90),
    height: Math.round((responsiveWidth(90) * 1) / 2.41),
    marginRight: responsiveWidth(5),
    marginLeft: "auto",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 12
    },
    shadowOpacity: 0.58,
    shadowRadius: 16.0,
    elevation: 20,
    marginBottom: responsiveHeight(2.5)
  },
  cardMemoriesA: {
    width: responsiveWidth(90),
    height: Math.round((responsiveWidth(90) * 1) / 2.41),
    marginRight: responsiveWidth(5),
    marginLeft: "auto",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 12
    },
    shadowOpacity: 0.58,
    shadowRadius: 16.0,
    elevation: 20
  },
  cardVideo: {
    marginRight: responsiveWidth(5),
    marginLeft: "auto",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 12
    },
    shadowOpacity: 0.58,
    shadowRadius: 16.0,
    elevation: 20,
    paddingBottom: responsiveHeight(5)
  },
  cardM: {
    resizeMode: "contain",
    width: responsiveWidth(90),
    height: Math.round((responsiveWidth(90) * 1) / 2.41),
    alignItems: "center"
  },
  cardD: {
    resizeMode: "contain",
    width: responsiveWidth(42.5),
    height: Math.round((responsiveWidth(45) * 1) / 1.78),
    alignItems: "center"
  },
  cardP: {
    resizeMode: "contain",
    width: responsiveWidth(42.5),
    height: Math.round((responsiveWidth(45) * 1) / 1.78),
    alignItems: "center"
  },
  cardC: {
    resizeMode: "contain",
    width: responsiveWidth(90),
    height: Math.round((responsiveWidth(90) * 1) / 2.41),
    alignItems: "center"
  },
  cardV: {
    width: responsiveWidth(90),
    height: Math.round((responsiveWidth(90) * 1) / 3.7),
    resizeMode: "contain",
    alignItems: "center"
  },
  container: {
    height: Math.round((responsiveWidth(100) * 1) / 1.95),
    justifyContent: "center",
    alignItems: "center",
    elevation: 24
  },
  menuBg: {
    width: responsiveWidth(100),
    height: Math.round((responsiveWidth(100) * 1) / 1.95),
    resizeMode: "contain",
    zIndex: 0
  },
  icon: {
    width: responsiveWidth(10),
    height: responsiveHeight(10),
    resizeMode: "contain",
    marginLeft: "auto",
    marginRight: responsiveWidth(20),
    marginTop: "auto",
    marginBottom: responsiveHeight(1)
  },
  slidingPanelLayoutStyle: {
    width: responsiveWidth(100),
    height: responsiveHeight(100),
    backgroundColor: "#00a7e1",
    justifyContent: "center",
    textAlign: "left",
    alignItems: "center",
    elevation: 24
  },
  textContainer: {
    width: responsiveWidth(90),
    marginTop: responsiveHeight(60),
    justifyContent: "flex-start",
    height: responsiveHeight(40),
    alignItems: "center"
  },
  textPanel: {
    fontSize: responsiveFontSize(2),
    color: "#fff",
    width: responsiveWidth(50)
  },
  textPanelHelp: {
    paddingTop: responsiveHeight(2),
    fontSize: responsiveFontSize(2),
    color: "#CDDB00",
    width: responsiveWidth(90),
    textAlign: "center",
    paddingBottom: responsiveHeight(4)
  },
  linePanel: {
    borderBottomColor: "white",
    borderBottomWidth: 2,
    width: responsiveWidth(80)
  },
  iconPanel: {
    flex: 2,
    height: responsiveHeight(2),
    resizeMode: "contain"
  },
  containerPanel: {
    flex: 2,
    flexDirection: "row"
  },
  avatarMenu: {
    height: responsiveHeight(20),
    width: responsiveHeight(20)
  },
  nameMenu: {
    color: "#FFF",
    fontSize: responsiveFontSize(3),
    textAlign: "center"
  }
});
