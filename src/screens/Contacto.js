import React, { Component } from "react";
import {
  Button,
  Text,
  View,
  ScrollView,
  StyleSheet,
  ImageBackground,
  Linking
} from "react-native";

const sierra = [
  {
    title: "I&A Impermeabilización & Acabados",
    subtitle: "Calle Corazón E2-131 y Avenida Napo (Sector Chimbacalle)",
    avatar_url: "https://imptek-dev.s3.amazonaws.com/industry.png",
    url: "https://www.google.com/maps/@-0.1923873,-78.4865596,15z"
  },
  {
    title: "Hidroasist S.A. ",
    subtitle: "VACA DE CASTRO OE4-428 Y PEDRO FREILE",
    avatar_url: "https://imptek-dev.s3.amazonaws.com/industry.png",

    url: ""
  },
  {
    title: "DISTRIBUIDORA IMPORTADORA HERNANDEZ",
    subtitle: "BILOXI, URB. VENCEDORES DE PICHINCHA CALLE OE9, S16-48",
    avatar_url: "https://imptek-dev.s3.amazonaws.com/industry.png",

    url: ""
  },
  {
    title: "Impterteco ",
    subtitle: "AV.10 DE AGOSTO N 44-76 Y AV. EL INCA",
    avatar_url: "https://imptek-dev.s3.amazonaws.com/industry.png",

    url: ""
  },
  {
    title: "Lamintech Cía. Ltda  ",
    subtitle:
      "Pasaje La Praga E 5-77 y Av. Interoceánica (Junto al CC Scala Shopping)",
    avatar_url: "https://imptek-dev.s3.amazonaws.com/industry.png",

    url: ""
  },
  {
    title: "Lamintech Cía. Ltda  ",
    subtitle:
      "Vía Circunvalación Etapa 1 lote 4.\n" +
      "Frente a la gas PRIMAX (Cerca al U. Laica Eloy Alfaro)",
    avatar_url: "https://imptek-dev.s3.amazonaws.com/industry.png",

    url: ""
  },
  {
    title: "ORO SOLIDO Construcciones Distribuciones Bienes Raices",
    subtitle: "Av. Quis Quis 0796 y Calle Oriente",
    avatar_url: "https://imptek-dev.s3.amazonaws.com/industry.png",

    url: ""
  },
  {
    title: "ECUAROOFING CIA. LTDA.",
    subtitle: "Av. América N34-507 y Hernández de Girón",
    avatar_url: "https://imptek-dev.s3.amazonaws.com/industry.png",

    url: ""
  },
  {
    title: "PROARQ",
    subtitle: "ALEMANIA N. 2969 Y ELOY ALFARO",
    avatar_url: "https://imptek-dev.s3.amazonaws.com/industry.png",

    url: ""
  }
];
const costa = [
  {
    title: "SHARPED S.A.",
    subtitle: "KM 11 VIA A  LA COSTA JUNTO A LA GASOLINERA PRIMAX",
    avatar_url: "https://imptek-dev.s3.amazonaws.com/industry.png",

    url: ""
  },
  {
    title: "SERVIDECONS S.A.",
    subtitle: "LOS VERGELES MZ 44 Sl 1 – 2",
    avatar_url: "https://imptek-dev.s3.amazonaws.com/industry.png",

    url: ""
  },
  {
    title: "SIMAC - ING FELIPE GARCÍA",
    subtitle: "HEREOS DE VERDELOMA 8-81 Y CORDERO",
    avatar_url: "https://imptek-dev.s3.amazonaws.com/industry.png",

    url: ""
  },
  {
    title: "FERREMUNDO S.A.",
    subtitle: "RIO DAULE SOLAR 28 MZ C-3 KM 16.5",
    avatar_url: "https://imptek-dev.s3.amazonaws.com/industry.png",

    url: ""
  },
  {
    title: "MULTIMETALES S.A",
    subtitle: "PEDRO MONCAYO #2322 Y FEBRES CORDERO",
    avatar_url: "https://imptek-dev.s3.amazonaws.com/industry.png",

    url: ""
  },
  {
    title: "MEGAHIERRO S.A",
    subtitle: "AV QUITO #2212 Y CAPITAN NAJERA",
    avatar_url: "https://imptek-dev.s3.amazonaws.com/industry.png",

    url: ""
  },
  {
    title: "JOHNNY ASTUDILLO",
    subtitle: "Lomas de Urdesa Callejon Cucalon 2312 y Colinas",
    avatar_url: "https://imptek-dev.s3.amazonaws.com/industry.png",

    url: ""
  },
  {
    title: "PRODUCTOS METALURGICOS S.A PROMESA",
    subtitle: "KM 5.5 VIA A DAULE",
    avatar_url: "https://imptek-dev.s3.amazonaws.com/industry.png",

    url: ""
  },
  {
    title: "REPRESENTACIONES INDUSTRIALES  CIA. LTDA. RINDA",
    subtitle: "ROCAFUERTE 713 ENTRE ORELLANA E IMBABURA",
    avatar_url: "https://imptek-dev.s3.amazonaws.com/industry.png",

    url: ""
  },
  {
    title: "VILLAVICENCIO AGILA PATZY CECILIA ",
    subtitle: "Sucre 10-102 y Azuay",
    avatar_url: "https://imptek-dev.s3.amazonaws.com/industry.png",

    url: ""
  },
  {
    title: "MONCAYO HERNANDEZ EDWIN ALDRIN",
    subtitle: "37AVA 1305 T GARCIA GOYENA",
    avatar_url: "https://imptek-dev.s3.amazonaws.com/industry.png",

    url: ""
  },
  {
    title: "CORPORACIÓN EL ROSADO S.A.",
    subtitle: "Km 9 1/2 de la vía a Daule",
    avatar_url: "https://imptek-dev.s3.amazonaws.com/industry.png",

    url: ""
  },
  {
    title: "FELIX RENDON",
    subtitle: "Acuarela Del Rio Mz 15 solar 11",
    avatar_url: "https://imptek-dev.s3.amazonaws.com/industry.png",

    url: ""
  },
  {
    title: "IMPERMEABILISA CIA. LTDA.",
    subtitle: "JUAN DE SALINAS Y SUCRE",
    avatar_url: "https://imptek-dev.s3.amazonaws.com/industry.png",

    url: ""
  },
  {
    title: "CASA FERRETERIA FONG S.A. FERREFONG",
    subtitle: "AV. 7 DE OCTUBRE OCTAVA Y NOVENA",
    avatar_url: "https://imptek-dev.s3.amazonaws.com/industry.png",

    url: ""
  }
];

import { ListItem } from "react-native-elements";
import {
  responsiveFontSize,
  responsiveWidth
} from "react-native-responsive-dimensions";

export default class ContactoScreen extends React.Component {
  static navigationOptions = {
    title: "Contacto",
    headerLeft: null,
    headerStyle: {
      backgroundColor: "#20A5DC"
    },
    headerTitleStyle: {
      color: "white"
    }
  };
  openURL(url) {
    Linking.openURL(url).catch(err => console.error("An error occurred", err));
  }

  render() {
    return (
      <ScrollView>
        <Text style={styles.titleItem}>Sierra</Text>
        {sierra.map((item, i) => (
          <ListItem
            key={i}
            title={item.title}
            subtitle={item.subtitle}
            subtitleStyle={styles.subtitle}
            leftAvatar={{ source: { uri: item.avatar_url } }}
            bottomDivider
            chevron
            onPress={() => this.openURL(item.url)}
          />
        ))}
        <Text style={styles.titleItem}>Costa</Text>
        {costa.map((item, i) => (
          <ListItem
            key={i}
            title={item.title}
            subtitle={item.subtitle}
            subtitleStyle={styles.subtitle}
            leftAvatar={{ source: { uri: item.avatar_url } }}
            bottomDivider
            chevron
          />
        ))}
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  titleItem: {
    fontSize: responsiveFontSize(3),
    marginLeft: responsiveWidth(5)
  },
  subtitle: {
    color: "gray"
  }
});
