import React from 'react';
import ReactNative from 'react-native';
import {Actions} from 'react-native-router-flux';
import PropTypes from 'prop-types';
import {User} from '../../types';
import Section from './Section';
import routes from '../../router/routes';
import Profile from './Profile';
import styles from './styles';

const {
    PureComponent
} = React;

const {
    Image,
    View,
    ImageBackground
} = ReactNative;

type Props = {
    user: User
}

type State = {
    route: any
}

export default class Menu extends PureComponent<void, Props, State> {
    static propTypes = {
        user: PropTypes.any
    };
    
    state:State = {
        route: null
    };
    
    _changeScene = (path:String, routerPath: string) => {
        this.setState({
            route: path
        });
        Actions.drawerClose();
        return Actions.reset(routerPath);
    };
    
    render () {
        const {route} = this.state;
        const {user} = this.props;
        // const profileTitle = account && account.user && account.user.name ? account.user.name : 'Profile';
        return (
            <View style={[ styles.container ]}>
                <ImageBackground
                    source={require('../../../assets/drawer_background07.png')}
                    style={styles.backgroundContainer}
                >
                    <Section
                        items={routes.map((routerRoute) => {
                            return {
                                value: routerRoute.name,
                                active: (!route && routerRoute.default) || route === routerRoute.path,
                                onPress: () => this._changeScene(routerRoute.path, routerRoute.pressPath),
                                onLongPress: () => this._changeScene(routerRoute.path, routerRoute.longPressPath)
                            };
                        })}
                    />
                    <Profile
                        image={<Image source={user.profileUrl} />}
                        primaryText={user.name}
                        secondaryText={user.email}
                    />
                </ImageBackground>
            </View>
        );
    }
}

