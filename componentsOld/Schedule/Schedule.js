import React from 'react';
import ReactNative, {Alert, Text} from 'react-native';
import {Actions} from 'react-native-router-flux';
import PropTypes from 'prop-types';
import styles from './styles';
import {Fab} from '../AnimatedButton';
import Card from '../Core/Card';
import AppText from '../Core/AppText';
import Post from '../Post';
import content from './content';
import Colors from '../../utils/Colors';
import Icons from '../Core/FontAwesomeIcons';
import {Header} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';

const {
    PureComponent
} = React;

const {
    ScrollView,
    Image,
    ImageBackground,
    StatusBar,
    View,
    TouchableHighlight,
    SafeAreaView
} = ReactNative;

type State = {
    startAnimation: boolean,
}

type Props = {
    logout: Function,
}

export default class Schedule extends PureComponent<void, Props, State> {
    static propTypes: Props = {
        logout: PropTypes.func,
    };
    
    state: State = {
        startAnimation: false,
    };
    
    _renderPost (post, i) {
        return (
            <Post
                avatarImage={post.profile}
                city={post.city}
                image={post.image}
                key={i}
                message={post.message}
                name={post.name}
                time={post.time}
                username={post.username}
            />
        );
    }
    
    _renderThumbnailRow = (source, i) => (
        <Card
            key={i}
            style={styles.thumbnailContainer}>
            <Image
                resizeMode={'cover'}
                source={source}
                style={styles.thumbnail}/>
        </Card>
    );
    
    _onPress () {
        this.setState({
            startAnimation: true
        });
    }
    
    _onAnimationComplete () {
        this.props.logout();
        Actions.login();
    }
    _frontHome () {
        Actions.reset('home');
    }
    _frontArticle () {
        Actions.reset('article');
    }
    render () {
        return (
            <ScrollView>
                <View style={styles.containerBlock}>
                    <Header
                        style={{height: 100, position: 'absolute'}}
                        leftComponent={
                            <Icon
                                name = {'angle-left'}
                                size = {30}
                                color = {'#ffffff'}
                                onPress={this._frontHome.bind(this)}
                                underlayColor={'#64b5f6'}
                            />
                        }
                        centerComponent={{text: 'Agenda', style: {color: '#fff'}}}
                        rightComponent={null}
                        containerStyle={{
                            backgroundColor: 'rgba(52, 52, 52, 0.7)',
                            justifyContent: 'space-around',
                        }}
                    />
                    <SafeAreaView style={{backgroundColor: '#9c9b9b'}}>
                        <View>
                            <View>
                                <TouchableHighlight
                                    onPress={this._frontArticle.bind(this)}
                                >
                                    <ImageBackground source={require('../../../assets/background_info_white.png')} style={styles.containerPost}>
                                        <Text style={styles.pTextDate}>08 de mayo, 09:30</Text>
                                        <Text style={styles.pTextTitle}>FORO Experiencia en la aplicación de productos Imptek Viabilidad</Text>
                                        <Text style={styles.iconContainerPost}>
                                            <Image source={require('../../../assets/view.png')} style={styles.iconPostView} />
                                            <Image source={require('../../../assets/bell.png')} style={styles.iconPostBell} />
                                        </Text>
                                        <Image source={require('../../../assets/btn_blue.png')} style={styles.iconBtnPost} />
                                    </ImageBackground>
                                </TouchableHighlight>
                            </View>
                            <View>
                                <TouchableHighlight
                                    onPress={this._frontHome.bind(this)}
                                >
                                    <ImageBackground source={require('../../../assets/background_info_green.png')} style={styles.containerPost}>
                                        <Text style={styles.pTextDateWhite}>08 de mayo, 09:30</Text>
                                        <Text style={styles.pTextTitleWhite}>FORO Experiencia en la aplicación de productos Imptek Viabilidad</Text>
                                        <Text style={styles.iconContainerPost}>
                                            <Image source={require('../../../assets/view.png')} style={styles.iconPostView} />
                                            <Image source={require('../../../assets/bell.png')} style={styles.iconPostBell} />
                                        </Text>
                                        <Image source={require('../../../assets/btn_gray.png')} style={styles.iconBtnPost} />
                                    </ImageBackground>
                                </TouchableHighlight>
                            </View>
                            <View>
                                <TouchableHighlight
                                    onPress={this._frontHome.bind(this)}
                                >
                                    <ImageBackground source={require('../../../assets/background_info_expositor.png')} style={styles.containerPost}>
                                        <Text style={styles.pTextDate}>08 de mayo, 09:30</Text>
                                        <Text style={styles.pTextTitle}>FORO Experiencia en la aplicación de productos Imptek Viabilidad</Text>
                                        <Text style={styles.iconContainerPost}>
                                            <Image source={require('../../../assets/view.png')} style={styles.iconPostView} />
                                            <Image source={require('../../../assets/bell.png')} style={styles.iconPostBell} />
                                        </Text>
                                        <Text style={styles.imgUserExhibitor}>
                                            <Image source={require('../../../assets/expositor.png')} style={styles.imgExhibitor} />
                                        </Text>
                                    </ImageBackground>
                                </TouchableHighlight>
                            </View>
                            <View>
                                <TouchableHighlight
                                    onPress={this._frontHome.bind(this)}
                                >
                                    <ImageBackground source={require('../../../assets/background_info_expositor.png')} style={styles.containerPost}>
                                        <Text style={styles.pTextDate}>08 de mayo, 09:30</Text>
                                        <Text style={styles.pTextTitle}>FORO Experiencia en la aplicación de productos Imptek Viabilidad</Text>
                                        <Text style={styles.iconContainerPost}>
                                            <Image source={require('../../../assets/view.png')} style={styles.iconPostView} />
                                            <Image source={require('../../../assets/bell.png')} style={styles.iconPostBell} />
                                        </Text>
                                        <Text style={styles.imgUserExhibitor}>
                                            <Image source={require('../../../assets/expositor.png')} style={styles.imgExhibitor} />
                                        </Text>
                                    </ImageBackground>
                                </TouchableHighlight>
                            </View>
                            <View>
                                <TouchableHighlight
                                    onPress={this._frontHome.bind(this)}
                                >
                                    <ImageBackground source={require('../../../assets/background_info_expositor.png')} style={styles.containerPost}>
                                        <Text style={styles.pTextDate}>08 de mayo, 09:30</Text>
                                        <Text style={styles.pTextTitle}>FORO Experiencia en la aplicación de productos Imptek Viabilidad</Text>
                                        <Text style={styles.iconContainerPost}>
                                            <Image source={require('../../../assets/view.png')} style={styles.iconPostView} />
                                            <Image source={require('../../../assets/bell.png')} style={styles.iconPostBell} />
                                        </Text>
                                        <Text style={styles.imgUserExhibitor}>
                                            <Image source={require('../../../assets/expositor.png')} style={styles.imgExhibitor} />
                                        </Text>
                                    </ImageBackground>
                                </TouchableHighlight>
                            </View>
                        </View>
                        <Fab
                            duration={1000}
                            onComplete={this._onAnimationComplete.bind(this)}
                            onPress={this._onPress.bind(this)}
                            rippleColor={Colors.fadedWhite}
                            startAnimation={this.state.startAnimation}
                            style={styles.fabButton}
                            width={50}
                        >
                            <Icons
                                color={Colors.white}
                                name="sign-out"
                                size={24}
                            />
                        </Fab>
                    </SafeAreaView>
                </View>
            </ScrollView>
        );
    }
}
