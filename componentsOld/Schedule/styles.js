import ReactNative from 'react-native';
import Colors from '../../utils/Colors';

const {
    StyleSheet,
    Dimensions,
    Platform
} = ReactNative;

const {width, height} = Dimensions.get('window');

const styles = StyleSheet.create({
    container: {
        backgroundColor: 'transparent',
        marginTop: 225,
        width: '100%',
        height: 210,
    },
    containerPost: {
        position: 'relative',
        backgroundColor: 'transparent',
        width,
        ...Platform.select({
            ios: {
                height: 195,
            },
            android: {
                height: 330,
            },
        }),
    },
    titleContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        margin: 0,
        left: 25,
        top: 100,
    },
    thumbnailContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        margin: 0,
        left: -138.5,
    },
    cardText: {
        color: Colors.grey,
        margin: 5,
        fontWeight: 'bold',
        fontSize: 10
    },
    marginTop0: {
        marginTop: 0
    },
    thumbnailOne: {
        width,
        height: 195
    },
    thumbnailTwo: {
        width: 160,
        height: 90
    },
    thumbnailThree: {
        width: 160,
        height: 143
    },
    thumbnailFour: {
        width: 160,
        height: 143
    },
    thumbnailFive: {
        width: 320,
        height: 85,
        position: 'absolute',
        right: 0,
        bottom: 65,
        alignItems: 'center',
        justifyContent: 'center',
    },
    fabButton: {
        position: 'absolute',
        bottom: 30,
        right: 30,
        borderRadius: 20,
    },
    textTitle: {
        flex: 2,
        width: 150,
        height: 50,
        margin: 10,
        color: '#ffffff',
        fontSize: 24,
        borderBottomColor: '#ffffff',
        borderBottomWidth: 1,
        marginBottom: 10,
    },
    bodyViewStyle: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    headerLayoutStyle: {
        width,
        height: 100,
        backgroundColor: 'transparent',
        justifyContent: 'center',
        alignItems: 'center',
    },
    slidingPanelLayoutStyle: {
        width,
        height,
        backgroundColor: '#00a7e1',
        justifyContent: 'center',
        alignItems: 'center',
    },
    commonTextStyle: {
        color: 'white',
        fontSize: 18,
    },
    commonTextStyleIcon: {
        color: 'white',
        fontSize: 18,
        position: 'absolute',
        bottom: 15,
        right: 60,
    },
    iconImage: {
        width: 40,
        height: 37,
    },
    pTextDate: {
        color: '#9c9b9b',
        top: 20,
        ...Platform.select({
            ios: {
                left: 20,
                fontSize: 16,
            },
            android: {
                left: 60,
                fontSize: 24,
            },
        }),
    },
    pTextDateWhite: {
        color: '#ffffff',
        top: 20,
        ...Platform.select({
            ios: {
                left: 20,
                fontSize: 16,
            },
            android: {
                left: 60,
                fontSize: 24,
            },
        }),
    },
    containerBlock: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'stretch',
        ...Platform.select({
            ios: {
                left: 0,
                top: 0,
            },
            android: {
                left: 0,
                top: 20,
            },
        }),
    },
    pTextTitle: {
        color: '#9c9b9b',
        fontWeight: 'bold',
        top: 25,
        ...Platform.select({
            ios: {
                left: 20,
                fontSize: 16,
                width: 220,
            },
            android: {
                left: 60,
                fontSize: 36,
                width: 300,
            },
        }),
    },
    pTextTitleWhite: {
        color: '#ffffff',
        fontWeight: 'bold',
        top: 25,
        ...Platform.select({
            ios: {
                left: 20,
                fontSize: 16,
                width: 220,
            },
            android: {
                left: 60,
                fontSize: 36,
                width: 300,
            },
        }),
    },
    iconPostView: {
        position: 'relative',
        top: 15,
        right: 15,
        ...Platform.select({
            ios: {
                width: 30,
                height: 30,
            },
            android: {
                width: 50,
                height: 50,
            },
        }),
    },
    iconPostBell: {
        position: 'relative',
        top: 15,
        right: 15,
        ...Platform.select({
            ios: {
                width: 30,
                height: 30,
            },
            android: {
                width: 50,
                height: 50,
            },
        }),
    },
    iconContainerPost: {
        position: 'relative',
        ...Platform.select({
            ios: {
                left: 20,
                top: 35,
            },
            android: {
                left: 60,
                top: 15,
            },
        }),
    },
    imgUserExhibitor: {
        position: 'absolute',
        ...Platform.select({
            ios: {
                right: 20,
                bottom: 15,
            },
            android: {
                right: 0,
                bottom: 0,
            },
        }),
    },
    imgExhibitor: {
        width: 120,
        height: 120,
    },
    textSalonOne: {
        position: 'absolute',
        right: 40,
        bottom: 40,
        width: 80,
        height: 20,
        backgroundColor: '#00a7e1',
        flex: 1, borderTopLeftRadius: 20, borderTopRightRadius: 20, overflow: 'hidden',
        textAlign: 'center',
        color: '#ffffff',
    },
    iconBtnPost: {
        position: 'absolute',
        right: 40,
        bottom: 40,
        width: 100,
        height: 24,
    },
});

export default styles;