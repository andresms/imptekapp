import React from 'react';
import {bindActionCreators} from 'redux';
import Map from './Map';
import {connect} from 'react-redux';
import {logout} from '../../actions';

const {
    Component
} = React;

const stateToProps = (state) => {
    return {
        account: state.account
    };
};

const dispatchToProps = (dispatch) => {
    return bindActionCreators({
        logout
    }, dispatch);
};

class MapContainer extends Component {
    render () {
        const {props} = this;
        return (
            <Map {...props}/>
        );
    }
}

export default connect(stateToProps, dispatchToProps)(MapContainer);
