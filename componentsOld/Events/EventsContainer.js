import React from 'react';
import {bindActionCreators} from 'redux';
import Events from './Events';
import {connect} from 'react-redux';
import {logout} from '../../actions';

const {
    Component
} = React;

const stateToProps = (state) => {
    return {
        account: state.account
    };
};

const dispatchToProps = (dispatch) => {
    return bindActionCreators({
        logout
    }, dispatch);
};

class EventsContainer extends Component {
    render () {
        const {props} = this;
        return (
            <Events {...props}/>
        );
    }
}

export default connect(stateToProps, dispatchToProps)(EventsContainer);
