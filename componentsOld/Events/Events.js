import React from 'react';
import ReactNative, {Alert, Text} from 'react-native';
import {Actions} from 'react-native-router-flux';
import PropTypes from 'prop-types';
import styles from './styles';
import {Fab} from '../AnimatedButton';
import Card from '../Core/Card';
import AppText from '../Core/AppText';
import Post from '../Post';
import content from './content';
import Colors from '../../utils/Colors';
import Icons from '../Core/FontAwesomeIcons';
import {Header} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';

const {
    PureComponent
} = React;

const {
    ScrollView,
    Image,
    ImageBackground,
    StatusBar,
    View,
    TouchableHighlight,
    SafeAreaView
} = ReactNative;

type State = {
    startAnimation: boolean,
}

type Props = {
    logout: Function,
}

export default class Events extends PureComponent<void, Props, State> {
    static propTypes: Props = {
        logout: PropTypes.func,
    };
    
    state: State = {
        startAnimation: false,
    };
    
    _renderPost (post, i) {
        return (
            <Post
                avatarImage={post.profile}
                city={post.city}
                image={post.image}
                key={i}
                message={post.message}
                name={post.name}
                time={post.time}
                username={post.username}
            />
        );
    }
    
    _renderThumbnailRow = (source, i) => (
        <Card
            key={i}
            style={styles.thumbnailContainer}>
            <Image
                resizeMode={'cover'}
                source={source}
                style={styles.thumbnail}/>
        </Card>
    );
    
    _onPress () {
        this.setState({
            startAnimation: true
        });
    }
    
    _onAnimationComplete () {
        this.props.logout();
        Actions.login();
    }
    _frontHome () {
        Actions.reset('home');
    }
    _frontArticle () {
        Actions.reset('article');
    }
    render () {
        return (
            <ScrollView>
                <View style={styles.containerBlock}>
                    <Header
                        style={{height: 100, position: 'absolute'}}
                        leftComponent={
                            <Icon
                                name = {'angle-left'}
                                size = {30}
                                color = {'#ffffff'}
                                onPress={this._frontHome.bind(this)}
                                underlayColor={'#00a7e1'}
                            />
                        }
                        centerComponent={{text: 'Próximos eventos', style: {color: '#fff'}}}
                        rightComponent={null}
                        containerStyle={{
                            backgroundColor: '#00a7e1',
                            justifyContent: 'space-around',
                        }}
                    />
                    <SafeAreaView style={{backgroundColor: '#00a7e1'}}>
                        <View style={{padding: 20}}>
                            <Text style={styles.textMap1}>Título Eventos</Text>
                            <Text style={styles.textMap2}>Lista de próximos eventos</Text>
                        </View>
                        <Fab
                            duration={1000}
                            onComplete={this._onAnimationComplete.bind(this)}
                            onPress={this._onPress.bind(this)}
                            rippleColor={Colors.fadedWhite}
                            startAnimation={this.state.startAnimation}
                            style={styles.fabButton}
                            width={50}
                        >
                            <Icons
                                color={Colors.white}
                                name="sign-out"
                                size={24}
                            />
                        </Fab>
                    </SafeAreaView>
                </View>
            </ScrollView>
        );
    }
}
