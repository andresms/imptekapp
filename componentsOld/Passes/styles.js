import ReactNative from 'react-native';
import Colors from '../../utils/Colors';

const {
    StyleSheet,
    Dimensions,
    Platform
} = ReactNative;

const {width, height} = Dimensions.get('window');

const styles = StyleSheet.create({
    container: {
        marginTop: 0,
        flex: 1,
        backgroundColor: '#ffffff',
        width,
        ...Platform.select({
            ios: {
                height: 100,
            },
            android: {
                height: 100,
            },
        }),
    },
    titleContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        margin: 0,
        left: 25,
        top: 0,
    },
    thumbnailContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        margin: 0,
        left: -138.5,
        top: 30,
    },
    cardText: {
        color: Colors.grey,
        margin: 5,
        fontWeight: 'bold',
        fontSize: 10
    },
    marginTop0: {
        marginTop: 0
    },
    thumbnailOne: {
        width: '90%',
        padding: 20,
        margin: 20,
        position: 'relative',
        ...Platform.select({
            ios: {
                height: 184,
                top: 60,
            },
            android: {
                height: 340,
                top: 270,
                left: 20,
            },
        }),
    },
    thumbnailTwo: {
        width: 160,
        height: 90
    },
    thumbnailThree: {
        width: 160,
        height: 143
    },
    thumbnailFour: {
        width: 160,
        height: 143
    },
    thumbnailFive: {
        width: 320,
        height: 85,
        position: 'absolute',
        right: 0,
        bottom: 65,
        alignItems: 'center',
        justifyContent: 'center',
    },
    fabButton: {
        position: 'absolute',
        bottom: 30,
        right: 30,
        borderRadius: 20,
    },
    textTitle: {
        width: '100%',
        height: 80,
        ...Platform.select({
            ios: {
                left: 20,
                top: 50,
            },
            android: {
                left: 50,
                top: 150,
            },
        }),
    },
    textTitleH1: {
        color: '#666666',
        ...Platform.select({
            ios: {
                fontSize: 28,
            },
            android: {
                fontSize: 50,
            },
        }),
    },
    textTitleH2: {
        color: '#666666',
        ...Platform.select({
            ios: {
                fontSize: 28,
                borderBottomWidth: 1,
                borderBottomColor: 'black',
                width: 400,
            },
            android: {
                fontSize: 50,
            },
        }),
    },
    textTitleH3: {
        color: '#666666',
        marginTop: 10,
        ...Platform.select({
            ios: {
                fontSize: 14,
            },
            android: {
                fontSize: 25,
            },
        }),
    },
    bodyViewStyle: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    headerLayoutStyle: {
        width,
        height: 200,
        backgroundColor: 'transparent',
        justifyContent: 'center',
        alignItems: 'center',
    },
    slidingPanelLayoutStyle: {
        width,
        height,
        backgroundColor: '#00a7e1',
        justifyContent: 'center',
        alignItems: 'center',
    },
    commonTextStyle: {
        color: 'white',
        fontSize: 18,
    },
    commonTextStyleIcon: {
        color: 'white',
        fontSize: 18,
        position: 'absolute',
        bottom: 15,
        right: 60,
    },
    iconImage: {
        width: 40,
        height: 37,
    },
    iconLogo: {
        width: 138,
        height: 60,
        position: 'absolute',
    },
    headerLogo: {
        left: 0,
        top: 0,
        position: 'absolute',
        height: 200,
    },
});

export default styles;