import React from 'react';
import {bindActionCreators} from 'redux';
import Passes from './Passes';
import {connect} from 'react-redux';
import {logout} from '../../actions';

const {
    Component
} = React;

const stateToProps = (state) => {
    return {
        account: state.account
    };
};

const dispatchToProps = (dispatch) => {
    return bindActionCreators({
        logout
    }, dispatch);
};

class PassesContainer extends Component {
    render () {
        const {props} = this;
        return (
            <Passes {...props}/>
        );
    }
}

export default connect(stateToProps, dispatchToProps)(PassesContainer);
