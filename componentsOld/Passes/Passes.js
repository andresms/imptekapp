import React from 'react';
import ReactNative, {Alert, alert} from 'react-native';
import {Text} from 'react-native';
import {Actions} from 'react-native-router-flux';
import PropTypes from 'prop-types';
import styles from './styles';
import {Fab} from '../AnimatedButton';
import Card from '../Core/Card';
import AppText from '../Core/AppText';
import Post from '../Post';
import content from './content';
import Colors from '../../utils/Colors';
import Icon from 'react-native-vector-icons/FontAwesome';
import {BottomNavigation} from "react-native-paper";
import SlidingPanel from 'react-native-sliding-up-down-panels';
import {Header} from 'react-native-elements';
import Icons from '../Core/FontAwesomeIcons';
import {Divider} from 'react-native-elements';

const {
    PureComponent
} = React;

const {
    ScrollView,
    Hr,
    Image,
    StatusBar,
    ImageBackground,
    View,
    TouchableHighlight,
    Dimensions
} = ReactNative;

const {width} = Dimensions.get('window');


type State = {
    startAnimation: boolean,
}

type Props = {
    logout: Function,
}

export default class Passes extends PureComponent<void, Props, State> {
    static propTypes: Props = {
        logout: PropTypes.func,
    };

    state: State = {
        startAnimation: false,
    };

    _renderPost (post, i) {
        return (
            <Post
                avatarImage={post.profile}
                city={post.city}
                image={post.image}
                key={i}
                message={post.message}
                name={post.name}
                time={post.time}
                username={post.username}
            />
        );
    }

    _renderThumbnailRow = (source, i) => (
        <Card
            key={i}
            style={styles.thumbnailContainer}>
            <Image
                resizeMode={'cover'}
                source={source}
                style={styles.thumbnail}/>
        </Card>
    );

    _onPress () {
        this.setState({
            startAnimation: true
        });
    }

    _onAnimationComplete () {
        this.props.logout();
        Actions.login();
    }

    alert = (msg) => {
        console.log(msg);
    }

    onDeleteBTN = () => {
        this.alert(' OnDelete');
    }
    _frontIntro () {
        Actions.reset('intro');
    }
    _frontLogin () {
        Actions.reset('login');
    }
    render () {
        return (
            <View style={{
                flex: 1,
                flexDirection: 'column',
                backgroundColor: '#ffffff',
            }}>
                <Header
                    leftComponent={
                        <Icon
                            name = {'angle-left'}
                            size = {30}
                            color = {'#666666'}
                            onPress={this._frontIntro.bind(this)}
                            underlayColor={'#64b5f6'}
                        />
                    }
                    centerComponent={{text: 'Imptek experience', style: {color: '#666666', fontSize: 22}}}
                    rightComponent={null}
                    containerStyle={{
                        backgroundColor: 'transparent',
                        justifyContent: 'space-around',
                    }}
                />
                <View style={styles.textTitle}>
                    <Text style={styles.textTitleH1}>Elige el tipo de</Text>
                    <Text style={styles.textTitleH2}>pase para la activación</Text>
                    <Divider style={{backgroundColor: '#ffffff', width: '80%', marginTop: 5, position: 'relative'}} />
                    <AppText style={styles.textTitleH3}>actividades</AppText>
                </View>
                <View>
                    <TouchableHighlight
                        onPress={this._frontLogin.bind(this)}
                    >
                        <Image
                            style={styles.thumbnailOne}
                            source={require('../../../assets/pase-vip.png')}
                        />
                    </TouchableHighlight>
                </View>
                <View>
                    <TouchableHighlight
                        onPress={this._frontLogin.bind(this)}
                    >
                        <Image
                            style={styles.thumbnailOne}
                            source={require('../../../assets/pase-general.png')}
                        />
                    </TouchableHighlight>
                </View>
            </View>
        );
    }
}
