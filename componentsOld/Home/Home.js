import React from 'react';
import ReactNative, {Alert, alert} from 'react-native';
import {Text} from 'react-native';
import {Actions} from 'react-native-router-flux';
import PropTypes from 'prop-types';
import styles from './styles';
import {Fab} from '../AnimatedButton';
import Card from '../Core/Card';
import AppText from '../Core/AppText';
import Post from '../Post';
import Colors from '../../utils/Colors';
import SlidingPanel from 'react-native-sliding-up-down-panels';
import {Divider, Header} from 'react-native-elements';
import Icons from '../Core/FontAwesomeIcons';

const {
    PureComponent
} = React;

const {
    ScrollView,
    Image,
    StatusBar,
    ImageBackground,
    View,
    TouchableHighlight,
    Platform
} = ReactNative;

type State = {
    startAnimation: boolean,
}

type Props = {
    logout: Function,
}

export default class Home extends PureComponent<void, Props, State> {
    static propTypes: Props = {
        logout: PropTypes.func,
    };

    state: State = {
        startAnimation: false,
    };

    _renderPost (post, i) {
        return (
            <Post
                avatarImage={post.profile}
                city={post.city}
                image={post.image}
                key={i}
                message={post.message}
                name={post.name}
                time={post.time}
                username={post.username}
            />
        );
    }

    _renderThumbnailRow = (source, i) => (
        <Card
            key={i}
            style={styles.thumbnailContainer}>
            <Image
                resizeMode={'cover'}
                source={source}
                style={styles.thumbnail}/>
        </Card>
    );

    _onPress () {
        this.setState({
            startAnimation: true
        });
    }

    _onAnimationComplete () {
        this.props.logout();
        Actions.login();
    }

    alert = (msg) => {
        console.log(msg);
    }

    onDeleteBTN = () => {
        this.alert(' OnDelete');
    }
    _frontSchedule () {
        Actions.reset('schedule');
    }
    _frontMap () {
        Actions.reset('map');
    }
    _frontMemories () {
        Actions.reset('memories');
    }
    _frontEvents () {
        Actions.reset('events');
    }
    render () {
        return (
            <View style={{flex: 1, flexDirection: 'row', backgroundColor: '#9c9b9b'}}>
                <View style={styles.textTitle}>
                    <Text style={styles.textTitleH1}>¿Qué deseas</Text>
                    <Text style={styles.textTitleH2}>hacer hoy?</Text>
                    <Divider style={{backgroundColor: '#ffffff', width: '80%', marginTop: 5, position: 'relative'}} />
                    <AppText style={styles.textTitleH3}>actividades</AppText>
                </View>
                <View style={styles.thumbnailContainer}>
                    <TouchableHighlight
                        onPress={this._frontSchedule.bind(this)}
                    >
                        <ImageBackground
                            source={require('../../../assets/category_convencion.png')} style={styles.thumbnailOne}>
                        </ImageBackground>
                    </TouchableHighlight>
                    <TouchableHighlight
                        onPress={this._frontMap.bind(this)}
                    >
                        <ImageBackground
                            source={require('../../../assets/category_agenda.png')} style={styles.thumbnailTwo}>
                        </ImageBackground>
                    </TouchableHighlight>
                </View>
                <View style={styles.thumbnailContainer}>
                    <TouchableHighlight
                        onPress={this._frontMemories.bind(this)}
                    >
                        <ImageBackground
                            source={require('../../../assets/category_noticias_home.png')} style={styles.thumbnailThree}>
                        </ImageBackground>
                    </TouchableHighlight>
                    <TouchableHighlight
                        onPress={this._frontEvents.bind(this)}
                    >
                        <ImageBackground
                            source={require('../../../assets/category_evento.png')} style={styles.thumbnailFour}>
                        </ImageBackground>
                    </TouchableHighlight>
                </View>
                <View style={styles.thumbnailContainer}>
                    <ImageBackground source={require('../../../assets/categoria_video.png')} style={styles.thumbnailFive}>
                    </ImageBackground>
                </View>
                <SlidingPanel
                    panelPosition = "top"
                    headerLayoutHeight = {300}
                    headerLayout = { () =>
                        <View style={styles.headerLayoutStyle}>
                            <ImageBackground source={require('../../../assets/menu_background.png')} style={styles.container}>
                                <Text style={{flex: 1, left: 20, ...Platform.select({ios: {top: 20,}, android: {top: 0,},})}}>
                                    <Image source={require('../../../assets/logo.png')} style={styles.iconLogo} />
                                </Text>
                                <Text style={styles.commonTextStyleIcon}>
                                    <Image source={require('../../../assets/icon.png')} style={styles.iconImage} />
                                </Text>
                            </ImageBackground>
                        </View>
                    }
                    slidingPanelLayout = { () =>
                        <View style={styles.slidingPanelLayoutStyle}>
                            <Text style={styles.commonTextStyle}>
                                <Icons
                                    color={Colors.white}
                                    name="home"
                                    size={24}
                                /> Inicio</Text>
                            <Text style={styles.commonTextStyle}>
                                <Icons
                                    color={Colors.white}
                                    name="user"
                                    size={24}
                                /> Tu cuenta</Text>
                            <Text style={styles.commonTextStyle}>
                                <Icons
                                    color={Colors.white}
                                    name="cogs"
                                    size={24}
                                /> Configuración</Text>
                            <Text style={styles.commonTextStyle}>
                                <Icons
                                    color={Colors.white}
                                    name="question-circle"
                                    size={24}
                                /> Ayuda</Text>
                        </View>
                    }
                />
                <Fab
                    duration={1000}
                    onComplete={this._onAnimationComplete.bind(this)}
                    onPress={this._onPress.bind(this)}
                    rippleColor={Colors.fadedWhite}
                    startAnimation={this.state.startAnimation}
                    style={styles.fabButton}
                    width={50}
                >
                    <Icons
                        color={Colors.white}
                        name="sign-out"
                        size={24}
                    />
                </Fab>
            </View>
        );
    }
}
