import React from 'react';
import ReactNative from 'react-native';
import {Alert, Text} from 'react-native';
import {Header} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Actions} from 'react-native-router-flux';
import SmartScrollView from 'react-native-smart-scroll-view';
// import dismissKeyboard from 'react-native-dismiss-keyboard';
import styles from './styles';
import {RectangularButton} from '../AnimatedButton';
import AnimatedLogo from '../AnimatedLogo';
import Colors from '../../utils/Colors';
import GroupTextField from '../Core/GroupTextField';
import AppText from '../Core/AppText';
import {validateEmail, validatePassword} from '../../lib/validator';
import {login} from '../../actions/login';

const {
    PureComponent,
} = React;

const {
    View,
    Image,
    Dimensions,
    StatusBar,
    Keyboard,
    LayoutAnimation,
    ImageBackground,
    SafeAreaView
} = ReactNative;

type State = {
    email: string,
    emailErrorMessage: string,
    password: string,
    passwordErrorMessage: string,
    isRecordUpdating: boolean,
    startAnimation: boolean,
    hideWelcomeMessage: boolean
}

const {width: WINDOW_WIDTH} = Dimensions.get('window');

export default class Login extends PureComponent<void, void, State> {
    state: State = {
        email: '',
        emailErrorMessage: null,
        password: '',
        passwordErrorMessage: null,
        startAnimation: false,
        isRecordUpdating: false,
        hideWelcomeMessage: false,
        titleText: 'Inicio de sesión',
        auth_token: ''
    };

    componentWillMount () {
        // Using keyboardWillShow/Hide looks 1,000 times better, but doesn't work on Android
        // TODO: Revisit this if Android begins to support - https://github.com/facebook/react-native/issues/3468
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow.bind(this));
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide.bind(this));
    }

    _keyboardDidShow () {
        this.setState({
            hideWelcomeMessage: true
        });
    }

    _keyboardDidHide () {
        this.setState({
            hideWelcomeMessage: false
        });
    }

    async _onPress () {
        Keyboard.dismiss();
        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        if (!this.state.emailErrorMessage && this.state.email && !this.state.passwordErrorMessage && this.state.password) {
            await this._updateRecord();
        }
        else {
            // smooth keyboard animation
            LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
            this._checkEmail(this.state.email);
            this._checkPassword(this.state.password);
        }

    }

    _onAnimationComplete () {
        this.setState({
            startAnimation: true
        });
        Actions.reset('recommendations');
    }

    async _updateRecord( ) {
        // this.setState({isRecordUpdating: true});
        // Hint -- can make service call and check;
        // Demo
        // this.setState({isRecordUpdating: false});
        this.setState({
            startAnimation: true
        });
    }



    _checkEmail (email) {
        let errorMessage = validateEmail(email);
        if (!errorMessage) {
            this.setState({email, emailErrorMessage: null});
        }
        else {
            this.setState({email, emailErrorMessage: errorMessage});
        }
    }

    _onEmailChange (email) {
        this._checkEmail(email);
    }

    _checkPassword (password) {
        let errorMessage = validatePassword(password);
        if (!errorMessage) {
            this.setState({password, passwordErrorMessage: null});
        }
        else {
            this.setState({password, passwordErrorMessage: errorMessage});
        }
    }

    _onPasswordChange (password) {
        this._checkPassword(password);
    }

    _frontIntro () {
        Actions.reset('intro');
    }

    _frontLogin () {
        Actions.reset('register');
    }

    _frontPasses () {
        Actions.reset('passes');
    }

    _login = async () => {
        fetch('https://www.zoomit.com.ec/api/v1/login/index.json', {
            method: 'post',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                'provider': 'username',
                'data': {
                    'email': this.state.email,
                    'password': this.state.password
                }
            })
        }).then((response) => response.json())
            .then((res) => {
                if (typeof (res.message) !== 'undefined')
                {

                    Alert.alert('Error', 'Error: '+ res.message);
                } else {
                    this.setState({auth_token: res.auth_token});
                    this._onAnimationComplete();
                }
            }).catch((error) => { console.error(error); });
    }

    render () {
        const {
            email,
            emailErrorMessage,
            passwordErrorMessage,
            password,
            startAnimation,
            isRecordUpdating,
            hideWelcomeMessage
        } = this.state;
        return (
            <View
                contentContainerStyle={styles.container}
            >
                <Header
                    leftComponent={
                        <Icon
                            name = {'angle-left'}
                            size = {30}
                            color = {'#666666'}
                            onPress={this._frontIntro.bind(this)}
                            underlayColor={'#64b5f6'}
                        />
                    }
                    centerComponent={{text: 'Inicio de sesión', style: {color: '#666666'}}}
                    rightComponent={null}
                    containerStyle={{
                        backgroundColor: 'transparent',
                    }}
                />
                <ImageBackground
                    style={styles.imageContainer}
                >
                    <View>
                        <AppText style={styles.logoContent}>
                            <Image source={require('../../../assets/logo_login.png')} style={styles.logoLogin} />
                        </AppText>
                    </View>
                    <View style={styles.leftSmallText}>
                        {
                            !hideWelcomeMessage &&
                            <AppText
                                style={styles.logoContent}>Bienvenido de nuevo,</AppText>
                        }
                        <Text
                            style={styles.leftSmallText}>Inicia sesión usando tu correo</Text>
                        <Text
                            style={styles.leftSmallText}></Text>
                    </View>
                    <View
                        style={styles.loginContainer}>
                        <GroupTextField
                            label="Correo electrónico"
                            onChangeText={this._onEmailChange.bind(this)}
                            value={email}
                            warningMessage={emailErrorMessage}
                            fontColor="#FFF"
                        />
                        <GroupTextField
                            label="Contraseña"
                            onChangeText={this._onPasswordChange.bind(this)}
                            secureTextEntry
                            value={password}
                            warningMessage={passwordErrorMessage}
                            fontColor="#FFF"
                        />
                        <AppText
                            style={styles.signUpTextRight}>¿Olvidaste tu contraseña?</AppText>
                        <RectangularButton
                            duration={1000}
                            fontColor="#FFF"
                            formCircle={true}
                            onComplete={this._onAnimationComplete.bind(this)}
                            onPress={this._login.bind(this)}
                            spinner={isRecordUpdating}
                            startAnimation={startAnimation}
                            text="Acceder"
                            width={150}
                            backgroundColor="#00a7e1"
                            style={styles.btnForm}

                        />
                        <AppText
                            style={styles.signUpText}>¿Nuevo usuario?</AppText>
                        <AppText
                            onPress={this._frontLogin.bind(this)}
                            style={styles.registerText}>Regístrate</AppText>
                    </View>
                </ImageBackground>
            </View>
        );
    }
}
