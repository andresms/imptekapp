import ReactNative from 'react-native';
import Colors from '../../utils/Colors';

const {
    Platform,
    StyleSheet,
    Dimensions,
} = ReactNative;

const {height: WINDOW_HEIGHT} = Dimensions.get('window');

const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: '100%',
    },
    headerContainer: {
        flex: 1,
        position: 'relative',
        flexDirection: 'column',
    },
    imageContainer: {
        position: 'relative',
        width: '100%',
        height: '100%',
        alignItems: 'center',
    },
    logoContainer: {
        textAlign: 'left',
        alignItems: 'center',
        justifyContent: 'center'
    },
    logoContent: {
        fontSize: 34,
        marginTop: 30,
        backgroundColor: Colors.transparent,
        color: '#666666',
        alignItems: 'center',
        justifyContent: 'center',
    },
    loginContainer: {
        backgroundColor: 'rgba(255, 255, 255, .1)',
        paddingBottom: 20,
        paddingLeft: 20,
        paddingRight: 20,
        flexDirection: 'column',
        top: 30,
        position: 'relative',
        width: '100%',
    },
    signUpText: {
        color: '#808080',
        fontSize: 14,
        textAlign: 'left',
        marginTop: 10
    },
    signUpTextRight: {
        color: '#808080',
        fontSize: 14,
        textAlign: 'right',
        marginTop: 10,
        marginBottom: 15,
    },
    registerText: {
        color: '#00a7e1',
        fontSize: 14,
        textAlign: 'left',
        marginTop: 10
    },
    btnForm: {
        marginRight: 40,
        marginLeft: 40,
        marginTop: 20,
        paddingTop: 20,
        paddingBottom: 20,
        borderRadius: 10,
        borderWidth: 1,
        top: 20,
    },
    leftSmallText: {
        color: '#808080',
        fontSize: 14,
        textAlign: 'left',
        marginTop: 5,
        paddingLeft: 15,
        paddingRight: 15,
        flexDirection: 'column'
    },
    logoLogin: {
        width: 200,
        height: 130,
    }
});

export default styles;
