import React from 'react';
import {bindActionCreators} from 'redux';
import Register from './Register';
import {connect} from 'react-redux';
import {login} from '../../actions';

const {
    Component
} = React;

const stateToProps = (state) => {
    return {
        account: state.account
    };
};

const dispatchToProps = (dispatch) => {
    return bindActionCreators({
        login
    }, dispatch);
};

class RegisterContainer extends Component {
    render () {
        const {props} = this;
        return (
            <Register {...props}/>
        );
    }
}

export default connect(stateToProps, dispatchToProps)(RegisterContainer);
