import ReactNative from 'react-native';
import Colors from '../../utils/Colors';

const {
    Platform,
    StyleSheet,
    Dimensions,
} = ReactNative;

const {height: WINDOW_HEIGHT} = Dimensions.get('window');

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
        height: '100%',
    },
    imageContainer: {
        flex: 1,
        // remove width and height to override fixed static size
        width: '100%',
        height: '100%',
        justifyContent: 'space-between',
        resizeMode: 'cover'
    },
    logoContainer: {
        textAlign: 'left',
        alignItems: 'center',
        justifyContent: 'center'
    },
    logoContent: {
        fontSize: 34,
        textAlign: 'center',
        marginTop: 10,
        backgroundColor: Colors.transparent,
        color: '#666666'
    },
    loginContainer: {
        backgroundColor: 'rgba(255, 255, 255, .1)',
        paddingBottom: 20,
        paddingLeft: 20,
        paddingRight: 20,
        width: '100%',
        height: WINDOW_HEIGHT,
        justifyContent: 'center',
    },
    signUpText: {
        color: '#808080',
        fontSize: 14,
        textAlign: 'left',
        marginTop: 10
    },
    registerText: {
        color: '#00a7e1',
        fontSize: 14,
        textAlign: 'left',
        marginTop: 10
    },
    btnForm: {
        marginRight: 40,
        marginLeft: 40,
        marginTop: 10,
        paddingTop: 20,
        paddingBottom: 20,
        borderRadius: 10,
        borderWidth: 1
    },
    leftSmallText: {
        color: '#808080',
        fontSize: 14,
        textAlign: 'left',
        marginTop: 5,
        paddingLeft: 15,
        paddingRight: 15,
        flexDirection: 'column'
    }
});

export default styles;
