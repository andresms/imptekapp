import React from 'react';
import ReactNative from 'react-native';
import {Alert, Text} from 'react-native';
import {Header} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Actions} from 'react-native-router-flux';
import SmartScrollView from 'react-native-smart-scroll-view';
// import dismissKeyboard from 'react-native-dismiss-keyboard';
import styles from './styles';
import {RectangularButton} from '../AnimatedButton';
import Colors from '../../utils/Colors';
import GroupTextField from '../Core/GroupTextField';
import AppText from '../Core/AppText';
import {validateEmail, validatePassword, validateName, validateMobile} from '../../lib/validator';

const {
    PureComponent,
} = React;

const {
    View,
    Image,
    Dimensions,
    StatusBar,
    Keyboard,
    LayoutAnimation,
    ImageBackground
} = ReactNative;

type State = {
    email: string,
    name: string,
    emailErrorMessage: string,
    nameErrorMessage: string,
    password: string,
    passwordErrorMessage: string,
    isRecordUpdating: boolean,
    startAnimation: boolean,
    hideWelcomeMessage: boolean
}

const {width: WINDOW_WIDTH} = Dimensions.get('window');

export default class Register extends PureComponent<void, void, State> {
    state: State = {
        email: '',
        name: '',
        emailErrorMessage: null,
        nameErrorMessage: null,
        password: '',
        passwordErrorMessage: null,
        startAnimation: false,
        isRecordUpdating: false,
        hideWelcomeMessage: false,
        titleText: 'Registro',
        auth_token: ''
    };

    componentWillMount () {
        // Using keyboardWillShow/Hide looks 1,000 times better, but doesn't work on Android
        // TODO: Revisit this if Android begins to support - https://github.com/facebook/react-native/issues/3468
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow.bind(this));
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide.bind(this));
    }

    _keyboardDidShow () {
        this.setState({
            hideWelcomeMessage: true
        });
    }

    _keyboardDidHide () {
        this.setState({
            hideWelcomeMessage: false
        });
    }

    async _onPressRegister () {
        Keyboard.dismiss();
        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        if (!this.state.emailErrorMessage && this.state.email && !this.state.passwordErrorMessage && this.state.password) {
            // await this._updateRecord();
        }
        else {
            // smooth keyboard animation
            LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
            this._checkEmail(this.state.email);
            this._checkName(this.state.name);
            this._checkPassword(this.state.password);
        }

    }

    _onAnimationComplete () {
        // Actions.reset("drawer");
    }

    async _updateRecord( ) {
        // this.setState({isRecordUpdating: true});
        // Hint -- can make service call and check;
        // Demo
        // this.setState({isRecordUpdating: false});
        this.setState({
            startAnimation: true
        });
    }



    _checkEmail (email) {
        let errorMessage = validateEmail(email);
        if (!errorMessage) {
            this.setState({email, emailErrorMessage: null});
        }
        else {
            this.setState({email, emailErrorMessage: errorMessage});
        }
    }

    _checkName (name) {
        let errorMessage = validateName(name);
        if (!errorMessage) {
            this.setState({name, nameErrorMessage: null});
        }
        else {
            this.setState({name, nameErrorMessage: errorMessage});
        }
    }

    _checkPhone (phone) {
        let errorMessage = validateMobile(phone);
        if (!errorMessage) {
            this.setState({phone, phoneErrorMessage: null});
        }
        else {
            this.setState({phone, phoneErrorMessage: errorMessage});
        }
    }

    _onNameChange (name) {
        this._checkName(name);
    }

    _onPhoneChange (phone) {
        this._checkPhone(phone);
    }

    _onEmailChange (email) {
        this._checkEmail(email);
    }

    _checkPassword (password) {
        let errorMessage = validatePassword(password);
        if (!errorMessage) {
            this.setState({password, passwordErrorMessage: null});
        }
        else {
            this.setState({password, passwordErrorMessage: errorMessage});
        }
    }

    _onPasswordChange (password) {
        this._checkPassword(password);
    }

    _backLogin () {
        Actions.reset('login');
    }

    _Register = async () => {
        fetch('https://www.zoomit.com.ec/api/v1/login/index.json', {
            method: 'post',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                'provider': 'username',
                'data': {
                    'email': this.state.email,
                    'password': this.state.password
                }
            })
        }).then((response) => response.json())
            .then((res) => {
                if (typeof (res.message) !== 'undefined')
                {

                    Alert.alert('Error', 'Error: '+ res.message);
                } else {
                    this.setState({auth_token: res.auth_token});
                    Alert.alert('Bienvenido', ' Has iniciado sesión');
                    Actions.reset('register');
                }
            }).catch((error) => { console.error(error); });
    }

    render () {
        const {
            email,
            name,
            phone,
            emailErrorMessage,
            nameErrorMessage,
            phoneErrorMessage,
            passwordErrorMessage,
            password,
            startAnimation,
            isRecordUpdating,
            hideWelcomeMessage
        } = this.state;
        return (
            <SmartScrollView
                contentContainerStyle={styles.container}
            >
                <ImageBackground
                    source={require('../../../assets/login_background.png')}
                    style={styles.imageContainer}
                >
                    <Header
                        leftComponent={
                            <Icon
                                name = "angle-left"
                                size = {30}
                                style={{color: '#666'}}
                                onPress={this._backLogin.bind(this)}
                            />
                        }
                        centerComponent={{text: 'Regístrate', style: {color: '#666666'}}}
                        rightComponent={null}
                        containerStyle={{
                            backgroundColor: 'transparent',
                            justifyContent: 'space-around',
                        }}
                    />
                    <View
                        style={styles.loginContainer}>
                        <AppText
                            style={styles.logoContent}>Registra tu cuenta,</AppText>
                        <Text
                            style={styles.leftSmallText}>Datos personales</Text>
                        <Text></Text>
                        <GroupTextField
                            label="Nombres completos"
                            onChangeText={this._onNameChange.bind(this)}
                            value={name}
                            warningMessage={nameErrorMessage}
                            fontColor="#FFF"
                        />
                        <GroupTextField
                            label="Número de celular"
                            onChangeText={this._onPhoneChange.bind(this)}
                            value={phone}
                            warningMessage={phoneErrorMessage}
                            fontColor="#FFF"
                        />
                        <GroupTextField
                            label="Correo electrónico"
                            onChangeText={this._onEmailChange.bind(this)}
                            value={email}
                            warningMessage={emailErrorMessage}
                            fontColor="#FFF"
                        />
                        <GroupTextField
                            label="Contraseña"
                            onChangeText={this._onPasswordChange.bind(this)}
                            secureTextEntry
                            value={password}
                            warningMessage={passwordErrorMessage}
                            fontColor="#FFF"
                        />
                        <RectangularButton
                            duration={1000}
                            fontColor="#FFF"
                            formCircle={true}
                            // onComplete={this._onAnimationComplete.bind(this)}
                            onPress={this._Register.bind(this)}
                            spinner={isRecordUpdating}
                            startAnimation={startAnimation}
                            text="Regístrarse"
                            width={200}
                            backgroundColor="#00a7e1"
                            style={styles.btnForm}
                        />
                        <AppText
                            style={styles.signUpText}>¿Ya tienes cuenta?</AppText>
                        <AppText
                            onPress={this._backLogin.bind(this)}
                            style={styles.registerText}>Iniciar sesión</AppText>
                    </View>
                </ImageBackground>
            </SmartScrollView>

        );
    }
}
