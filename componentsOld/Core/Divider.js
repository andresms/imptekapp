import React from 'react';
import ReactNative from 'react-native';
import PropTypes from 'prop-types';

const {
    PureComponent
} = React;

const {
    View
} = ReactNative;

type Props = {
    inset: boolean,
    style: any
}

export default class Divider extends PureComponent<Props, Props, void> {
    
    static propTypes = {
        inset: PropTypes.bool,
        style: PropTypes.any
    };
    
    static defaultProps = {
        inset: false,
    };
    
    render () {
        const {inset, style} = this.props;
        
        return (
            <View
                style={[
                    styles.divider,
                    inset && {marginHorizontal: 72}, {
                        backgroundColor: 'rgba(0,0,0,.12)'
                    },
                    style
                ]}
            />
        );
    }
}

const styles = {
    divider: {
        height: 1
    }
};
