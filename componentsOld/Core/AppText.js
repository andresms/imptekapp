import React from 'react';
import ReactNative from 'react-native';
import PropTypes from 'prop-types';

const {
    PureComponent
} = React;

const {
    StyleSheet,
    Text,
} = ReactNative;

const styles = StyleSheet.create({
    text: {
        fontSize: 14,
    },
});

type Props = {
    children?: React.Element<*>;
    style?: any;
}

export default class AppText extends PureComponent<void, Props, void> {
    static propTypes = {
        children: PropTypes.node.isRequired,
        style: Text.propTypes.style,
    };

    _root: Object;

    setNativeProps: Function = (nativeProps: Props): void => {
        this._root.setNativeProps(nativeProps);
    };

    render() {
        return (
            <Text
                {...this.props}
                ref={c => (this._root = c)}
                style={[styles.text, this.props.style]}
            >
                {this.props.children}
            </Text>
        );
    }
}
