import React from 'react';
import ReactNative from 'react-native';
import EvilIcons from 'react-native-vector-icons/EvilIcons';
import PropTypes from 'prop-types';

const {
    PureComponent
} = React;

type Props = {
    style?: any;
}

export default class MaterialIcon extends PureComponent<void, Props, void> {
    static propTypes = {
        style: PropTypes.any
    };
    
    setNativeProps (nativeProps: Props) {
        this._root.setNativeProps(nativeProps);
    }
    
    _root: Object;
    
    render () {
        const {props} = this;
        return (
            <EvilIcons
                allowFontScaling={false}
                ref={c => (this._root = c)}
                {...props}
            />
        );
    }
}

