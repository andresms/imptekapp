import ReactNative from 'react-native';
import Colors from '../../utils/Colors';

const {
    StyleSheet,
    Dimensions,
    Platform
} = ReactNative;

const {width, height} = Dimensions.get('window');

const styles = StyleSheet.create({
    container: {
        marginTop: 0,
        flex: 1,
        width,
        ...Platform.select({
            ios: {
                height: 210,
            },
            android: {
                height: 365,
            },
        }),
    },
    titleContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        margin: 0,
        left: 25,
        top: 130,
    },
    thumbnailContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        margin: 0,
        left: -138.5,
        top: 30,
    },
    cardText: {
        color: Colors.grey,
        margin: 5,
        fontWeight: 'bold',
        fontSize: 10
    },
    marginTop0: {
        marginTop: 0
    },
    thumbnailOne: {
        width: '100%',
        position: 'relative',
        ...Platform.select({
            ios: {
                height: 245,
            },
            android: {
                height: 340,
                top: 120,
            },
        }),
    },
    thumbnailTwo: {
        width: 160,
        height: 90
    },
    thumbnailThree: {
        width: 160,
        height: 143
    },
    thumbnailFour: {
        width: 160,
        height: 143
    },
    thumbnailFive: {
        width: 320,
        height: 85,
        position: 'absolute',
        right: 0,
        bottom: 65,
        alignItems: 'center',
        justifyContent: 'center',
    },
    fabButton: {
        position: 'absolute',
        bottom: 30,
        right: 30,
        borderRadius: 20,
    },
    textTitle: {
        width: '100%',
        height: 80,
        ...Platform.select({
            ios: {
                left: 20,
            },
            android: {
                left: 50,
            },
        }),
    },
    textTitleH1: {
        color: '#ffffff',
        ...Platform.select({
            ios: {
                fontSize: 28,
            },
            android: {
                fontSize: 50,
            },
        }),
    },
    textTitleH2: {
        color: '#ffffff',
        ...Platform.select({
            ios: {
                fontSize: 28,
                borderBottomWidth: 1,
                borderBottomColor: 'black',
                width: 400,
            },
            android: {
                fontSize: 50,
            },
        }),
    },
    textTitleH3: {
        color: '#ffffff',
        marginTop: 10,
        ...Platform.select({
            ios: {
                fontSize: 14,
            },
            android: {
                fontSize: 25,
            },
        }),
    },
    bodyViewStyle: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    headerLayoutStyle: {
        width,
        backgroundColor: 'transparent',
        justifyContent: 'center',
        alignItems: 'center',
        ...Platform.select({
            ios: {
                height: 250,
            },
            android: {
                height: 300,
            },
        }),
    },
    slidingPanelLayoutStyle: {
        width,
        height,
        backgroundColor: '#00a7e1',
        justifyContent: 'center',
        textAlign: 'left',
        marginTop: 20,
        top: 20,
    },
    commonTextStyle: {
        color: 'white',
        fontSize: 18,
        textAlign: 'left',
        paddingLeft: 20,
        paddingTop: 15,
    },
    commonTextStyleIcon: {
        color: 'white',
        fontSize: 18,
        position: 'absolute',
        ...Platform.select({
            ios: {
                bottom: 75,
                right: 75,
            },
            android: {
                bottom: 0,
                right: 175,
            },
        }),
    },
    iconImage: {
        width: 40,
        height: 37,
    },
    iconLogo: {
        position: 'absolute',
        ...Platform.select({
            ios: {
                width: 110,
                height: 48,
            },
            android: {
                width: 170,
                height: 74,
            },
        }),
    },
    headerLogo: {
        left: 0,
        top: 0,
        position: 'absolute',
        height: 300,
    },
});

export default styles;