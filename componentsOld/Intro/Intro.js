import React from 'react';
import ReactNative, {Alert, alert} from 'react-native';
import {Text} from 'react-native';
import {Actions} from 'react-native-router-flux';
import PropTypes from 'prop-types';
import styles from './styles';
import {Fab} from '../AnimatedButton';
import Card from '../Core/Card';
import AppText from '../Core/AppText';
import Post from '../Post';
import Colors from '../../utils/Colors';
import SlidingPanel from 'react-native-sliding-up-down-panels';
import Icons from '../Core/FontAwesomeIcons';
import {Divider} from 'react-native-elements';

const {
    PureComponent
} = React;

const {
    ScrollView,
    Hr,
    Image,
    StatusBar,
    ImageBackground,
    View,
    TouchableHighlight,
    Dimensions,
    Platform,
    SafeAreaView
} = ReactNative;

const {width} = Dimensions.get('window');

type State = {
    startAnimation: boolean,
}

type Props = {
    logout: Function,
}

export default class Intro extends PureComponent<void, Props, State> {
    static propTypes: Props = {
        logout: PropTypes.func,
    };

    state: State = {
        startAnimation: false,
    };

    _renderPost (post, i) {
        return (
            <Post
                avatarImage={post.profile}
                city={post.city}
                image={post.image}
                key={i}
                message={post.message}
                name={post.name}
                time={post.time}
                username={post.username}
            />
        );
    }

    _renderThumbnailRow = (source, i) => (
        <Card
            key={i}
            style={styles.thumbnailContainer}>
            <Image
                resizeMode={'cover'}
                source={source}
                style={styles.thumbnail}/>
        </Card>
    );

    _onPress () {
        this.setState({
            startAnimation: true
        });
    }

    _onAnimationComplete () {
        this.props.logout();
        Actions.login();
    }

    alert = (msg) => {
        console.log(msg);
    }

    onDeleteBTN = () => {
        this.alert(' OnDelete');
    }
    _frontLogin () {
        Actions.reset('login');
    }

    render () {
        return (
            <SafeAreaView style={{
                flex: 1,
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'stretch',
                backgroundColor: '#9c9b9b',
            }}>
                <View style={styles.textTitle}>
                    <Text style={styles.textTitleH1}>¿Qué deseas</Text>
                    <Text style={styles.textTitleH2}>hacer hoy?</Text>
                    <Divider style={{backgroundColor: '#ffffff', width: '80%', marginTop: 5, position: 'relative'}} />
                    <AppText style={styles.textTitleH3}>actividades</AppText>
                </View>
                <View>
                    <TouchableHighlight
                        onPress={this._frontLogin.bind(this)}
                    >
                        <Image
                            style={styles.thumbnailOne}
                            source={require('../../../assets/btn_intro.png')}
                        />
                    </TouchableHighlight>
                </View>
                <SlidingPanel
                    panelPosition = "top"
                    headerLayoutHeight = {300}
                    headerLayout = { () =>
                        <View style={styles.headerLayoutStyle}>
                            <ImageBackground source={require('../../../assets/menu_background.png')} style={styles.container}>
                                <Text style={{flex: 1, left: 20, ...Platform.select({ios: {top: 20,}, android: {top: 0,},})}}>
                                    <Image source={require('../../../assets/logo.png')} style={styles.iconLogo} />
                                </Text>
                                <Text style={styles.commonTextStyleIcon}>
                                    <Image source={require('../../../assets/icon.png')} style={styles.iconImage} />
                                </Text>
                            </ImageBackground>
                        </View>
                    }
                    slidingPanelLayout = { () =>
                        <SafeAreaView style={styles.slidingPanelLayoutStyle}>
                            <Text style={styles.commonTextStyle}>
                                <Icons
                                    color={Colors.white}
                                    name="home"
                                    size={24}
                                /> Inicio</Text>
                            <Text style={styles.commonTextStyle}>
                                <Icons
                                    color={Colors.white}
                                    name="user"
                                    size={24}
                                /> Tu cuenta</Text>
                            <Text style={styles.commonTextStyle}>
                                <Icons
                                    color={Colors.white}
                                    name="cogs"
                                    size={24}
                                /> Configuración</Text>
                            <Text style={styles.commonTextStyle}>
                                <Icons
                                    color={Colors.white}
                                    name="question-circle"
                                    size={24}
                                /> Ayuda</Text>
                        </SafeAreaView>
                    }
                />
            </SafeAreaView>
        );
    }
}
