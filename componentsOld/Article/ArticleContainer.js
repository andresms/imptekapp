import React from 'react';
import {bindActionCreators} from 'redux';
import Article from './Article';
import {connect} from 'react-redux';
import {logout} from '../../actions';

const {
    Component
} = React;

const stateToProps = (state) => {
    return {
        account: state.account
    };
};

const dispatchToProps = (dispatch) => {
    return bindActionCreators({
        logout
    }, dispatch);
};

class ArticleContainer extends Component {
    render () {
        const {props} = this;
        return (
            <Article {...props}/>
        );
    }
}

export default connect(stateToProps, dispatchToProps)(ArticleContainer);
