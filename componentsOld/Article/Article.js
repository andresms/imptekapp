import React from 'react';
import ReactNative, {Alert, Text} from 'react-native';
import {Actions} from 'react-native-router-flux';
import PropTypes from 'prop-types';
import styles from './styles';
import {Fab} from '../AnimatedButton';
import Card from '../Core/Card';
import AppText from '../Core/AppText';
import Post from '../Post';
import content from './content';
import Colors from '../../utils/Colors';
import Icons from '../Core/FontAwesomeIcons';
import {Divider, Header} from 'react-native-elements';

const {
    PureComponent
} = React;

const {
    ScrollView,
    Image,
    ImageBackground,
    StatusBar,
    View,
    TouchableHighlight,
    SafeAreaView
} = ReactNative;

type State = {
    startAnimation: boolean,
}

type Props = {
    logout: Function,
}

export default class Article extends PureComponent<void, Props, State> {
    static propTypes: Props = {
        logout: PropTypes.func,
    };
    
    state: State = {
        startAnimation: false,
    };
    
    _renderPost (post, i) {
        return (
            <Post
                avatarImage={post.profile}
                city={post.city}
                image={post.image}
                key={i}
                message={post.message}
                name={post.name}
                time={post.time}
                username={post.username}
            />
        );
    }
    
    _renderThumbnailRow = (source, i) => (
        <Card
            key={i}
            style={styles.thumbnailContainer}>
            <Image
                resizeMode={'cover'}
                source={source}
                style={styles.thumbnail}/>
        </Card>
    );
    
    _onPress () {
        this.setState({
            startAnimation: true
        });
    }
    
    _onAnimationComplete () {
        this.props.logout();
        Actions.login();
    }
    _frontSchedule () {
        Actions.reset('schedule');
    }
    render () {
        return (
            <ScrollView>
                <SafeAreaView style={styles.containerBlock}>
                    <Header
                        style={{height: 100, position: 'absolute'}}
                        leftComponent={
                            <Icons
                                name = {'angle-left'}
                                size = {30}
                                color = {'#ffffff'}
                                onPress = {this._frontSchedule.bind(this)}
                                underlayColor={'#64b5f6'}
                            />
                        }
                        centerComponent={{text: 'Expositor', style: {color: '#fff'}}}
                        rightComponent={null}
                        containerStyle={{
                            backgroundColor: '#00a7e1',
                            justifyContent: 'space-around',
                        }}
                    />
                    <SafeAreaView style={{backgroundColor: '#00a7e1'}}>
                        <ImageBackground source={require('../../../assets/background_article.jpg')} style={styles.containerPost}>
                            <Text style={styles.pTextDate}>08 de mayo, 09:30</Text>
                            <Text style={styles.pTextTitle}>FORO Experiencia en la aplicación de productos Imptek Viabilidad</Text>
                            <Text style={styles.iconContainerPost}>
                                <Image source={require('../../../assets/view.png')} style={styles.iconPostView} />
                                <Image source={require('../../../assets/bell.png')} style={styles.iconPostBell} />
                            </Text>
                            <Text style={styles.imgUserExhibitor}>
                                <Image source={require('../../../assets/expositor.png')} style={styles.imgExhibitor} />
                            </Text>
                            <Divider style={{backgroundColor: '#ffffff', width: '100%', marginTop: 75, position: 'relative'}} />
                            <Text style={styles.pTextTitleArticle}>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer
                                took a galley of type and scrambled it to make a type specimen book. It has survived not only five
                                centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was
                                popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,
                                and more recently with desktop publishing software like Aldus PageMaker including versions of
                                Lorem Ipsum.</Text>
                            <Text style={styles.pTextTitleArticle}>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer
                                took a galley of type and scrambled it to make a type specimen book. It has survived not only five
                                centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was
                                popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,
                                and more recently with desktop publishing software like Aldus PageMaker including versions of
                                Lorem Ipsum.</Text>
                        </ImageBackground>
                    </SafeAreaView>
                </SafeAreaView>
            </ScrollView>
        );
    }
}
