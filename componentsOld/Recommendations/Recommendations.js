import React from 'react';
import ReactNative, {Alert, alert} from 'react-native';
import {Text} from 'react-native';
import {Actions} from 'react-native-router-flux';
import PropTypes from 'prop-types';
import styles from './styles';
import {Fab} from '../AnimatedButton';
import Card from '../Core/Card';
import AppText from '../Core/AppText';
import Post from '../Post';
import content from './content';
import Colors from '../../utils/Colors';
import Icon from 'react-native-vector-icons/FontAwesome';
import {BottomNavigation} from "react-native-paper";
import SlidingPanel from 'react-native-sliding-up-down-panels';
import {Header} from 'react-native-elements';
import Icons from '../Core/FontAwesomeIcons';
import {Divider} from 'react-native-elements';
import AppIntroSlider from 'react-native-app-intro-slider';
import Login from '../Auth/Login';
import Home from '../Home/Home';

const {
    PureComponent
} = React;

const {
    ScrollView,
    Hr,
    Image,
    StatusBar,
    ImageBackground,
    View,
    TouchableHighlight,
    Dimensions,
    Platform,
    StyleSheet,
    SafeAreaView
} = ReactNative;

const {width} = Dimensions.get('window');

const style = StyleSheet.create({
    buttonCircle: {
        width: 40,
        height: 40,
        backgroundColor: 'rgba(0, 0, 0, .2)',
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
        top: 15,
    },
    image: {
        width,
        height: '100%',
        position: 'absolute',
        bottom: 0,
    }
});

const slides = [
    {
        image: require('./assets/10.jpg'),
        imageStyle: style.image,
        backgroundColor: '#ffffff',
    },
    {
        image: require('./assets/1.jpg'),
        imageStyle: style.image,
        backgroundColor: '#ffffff',
    },
    {
        image: require('./assets/2.jpg'),
        imageStyle: style.image,
        backgroundColor: '#ffffff',
    },
    {
        image: require('./assets/3.jpg'),
        imageStyle: style.image,
        backgroundColor: '#ffffff',
    },
    {
        image: require('./assets/4.jpg'),
        imageStyle: style.image,
        backgroundColor: '#ffffff',
    },
    {
        image: require('./assets/5.jpg'),
        imageStyle: style.image,
        backgroundColor: '#ffffff',
    },
    {
        image: require('./assets/6.jpg'),
        imageStyle: style.image,
        backgroundColor: '#ffffff',
    },
    {
        image: require('./assets/7.jpg'),
        imageStyle: style.image,
        backgroundColor: '#ffffff',
    },
    {
        image: require('./assets/8.jpg'),
        imageStyle: style.image,
        backgroundColor: '#ffffff',
    },
    {
        image: require('./assets/9.jpg'),
        imageStyle: style.image,
        backgroundColor: '#ffffff',
    },
];

type State = {
    startAnimation: boolean,
}

type Props = {
    logout: Function,
}

export default class Recommendations extends PureComponent<void, Props, State> {
    static propTypes: Props = {
        logout: PropTypes.func,
    };

    state: State = {
        startAnimation: false,
        showRealApp: false,
        hideNextButton: true,
    };

    _renderPost (post, i) {
        return (
            <Post
                avatarImage={post.profile}
                city={post.city}
                image={post.image}
                key={i}
                message={post.message}
                name={post.name}
                time={post.time}
                username={post.username}
            />
        );
    }

    _renderThumbnailRow = (source, i) => (
        <Card
            key={i}
            style={styles.thumbnailContainer}>
            <Image
                resizeMode={'cover'}
                source={source}
                style={styles.thumbnail}/>
        </Card>
    );

    _onPress () {
        this.setState({
            startAnimation: true
        });
    }

    _onAnimationComplete () {
        this.props.logout();
        Actions.login();
    }

    alert = (msg) => {
        console.log(msg);
    }

    onDeleteBTN = () => {
        this.alert(' OnDelete');
    }
    _frontLogin () {
        Actions.reset('home');
    }

    _onDone = () => {
        // User finished the introduction. Show real app through
        // navigation or simply by controlling state
        this.setState({showRealApp: true});
    }

    _renderNextButton = () => {
        this.setState({hideNextButton: true});
    }

    _renderDoneButton = () => {
        return (
            <View style={style.buttonCircle}>
                <Icon
                    name = {'check'}
                    size = {30}
                    color = {'#ffffff'}
                />
            </View>
        );
    }

    render () {
        if (this.state.showRealApp) {
            const {props} = this;
            return (
                <Home {...props}/>
            );
        } else {
            return (
                <AppIntroSlider
                    renderNextButton="Siguiente"
                    slides={slides}
                    renderDoneButton={this._renderDoneButton}
                    renderNextButton={this._renderNextButton}
                    onDone={this._onDone}
                />
            );

        }
    }
}
