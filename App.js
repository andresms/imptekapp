import React from "react";
import { Button, Text, View } from "react-native";
import Firebase from "./src/firebase";
import {
  createSwitchNavigator,
  createBottomTabNavigator,
  createAppContainer,
  createStackNavigator
} from "react-navigation";
import {
  responsiveHeight,
  responsiveWidth,
  responsiveFontSize
} from "react-native-responsive-dimensions";
import Ionicons from "react-native-vector-icons/Ionicons";
import InicioScreen from "./src/screens/Inicio";
import HomeExpoScreen from "./src/screens/Convencion/HomeExpo";
import MemoriasScreen from "./src/screens/Convencion/Memorias";
import MaterialDescargableScreen from "./src/screens/Convencion/MaterialDescargable";
import PromocionesScreen from "./src/screens/Convencion/Promociones";
import VideoScreen from "./src/screens/Convencion/Video";
import CotizadorScreen from "./src/screens/Cotizador/Cotizador";
import ImperScreen from "./src/screens/Cotizador/Imper";
import ProcesoScreen from "./src/screens/Cotizador/Proceso";
import ContactoScreen from "./src/screens/Contacto";
import PerfilScreen from "./src/screens/Perfil";
import UserLoginScreen from "./src/screens/User/Login";
import Navigator from "./src/screens/User/UserNavigator";
import * as firebase from "firebase";

const HomeNavigator = createStackNavigator({
  Welcome: InicioScreen
});
const ExpoNavigator = createStackNavigator({
  ExpoHome: HomeExpoScreen,
  Memories: MemoriasScreen,
  MaterialDescargable: MaterialDescargableScreen,
  Promociones: PromocionesScreen,
  Video: VideoScreen
});
const CotizadorNavigator = createStackNavigator({
  Cotizador: CotizadorScreen,
  Imper: ImperScreen,
  Process: ProcesoScreen
});
const PerfilNavigator = createStackNavigator({
  Perfil: PerfilScreen
});
const ContactoNavigator = createStackNavigator({
  Contacto: ContactoScreen
});
const AppNavigator = createBottomTabNavigator(
  {
    Home: {
      screen: HomeNavigator,
      navigationOptions: {
        tabBarLabel: ({ tintColor }) => (
          <Text style={{ fontSize: 10, color: tintColor, textAlign: "center" }}>
            home
          </Text>
        )
      }
    },
    Cotizador: {
      screen: CotizadorNavigator,
      navigationOptions: {
        tabBarLabel: ({ tintColor }) => (
          <Text style={{ fontSize: 10, color: tintColor, textAlign: "center" }}>
            cotizador
          </Text>
        )
      }
    },
    Perfil: {
      screen: PerfilNavigator,
      navigationOptions: {
        tabBarLabel: ({ tintColor }) => (
          <Text style={{ fontSize: 10, color: tintColor, textAlign: "center" }}>
            perfil
          </Text>
        )
      }
    },
    Contacto: {
      screen: ContactoNavigator,
      navigationOptions: {
        tabBarLabel: ({ tintColor }) => (
          <Text style={{ fontSize: 10, color: tintColor, textAlign: "center" }}>
            contacto
          </Text>
        )
      }
    },
    Novena: {
      screen: ExpoNavigator,
      navigationOptions: {
        tabBarLabel: ({ tintColor }) => (
          <Text style={{ fontSize: 10, color: tintColor, textAlign: "center" }}>
            convención
          </Text>
        )
      }
    }
  },

  {
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, horizontal, tintColor }) => {
        const { routeName } = navigation.state;
        let IconComponent = Ionicons;
        let iconName;
        if (routeName === "Home") {
          iconName = `md-home${focused ? "" : ""}`;
        } else if (routeName === "Novena") {
          iconName = `md-calendar${focused ? "" : ""}`;
        } else if (routeName === "Cotizador") {
          iconName = `md-calculator${focused ? "" : ""}`;
        } else if (routeName === "Perfil") {
          iconName = `md-person${focused ? "" : ""}`;
        } else if (routeName === "Contacto") {
          iconName = `md-call${focused ? "" : ""}`;
        }

        // You can return any component that you like here!
        return <IconComponent name={iconName} size={25} color={tintColor} />;
      }
    }),
    tabBarOptions: {
      activeTintColor: "#00a7e1",
      inactiveTintColor: "#989898"
      // Elimina la franja blanca debajo del tabBar en iPhone X y similares
      // safeAreaInset: { bottom: "never", top: "never" }
    }
  }
);

const InitialNavigator = createSwitchNavigator({
  App: AppNavigator
});

const AppContainer = createAppContainer(InitialNavigator);

class App extends React.Component {
  constructor(props) {
    super(props);
    Firebase.init();
    this.componentDidMount = this.componentDidMount.bind(this);
    this.state = {
      isLogged: null,
      ready: false
    };
  }
  async componentDidMount() {
    firebase
      .auth()
      .onAuthStateChanged(user => {
        user
          ? this.setState(() => ({ isLogged: true }))
          : this.setState(() => ({ isLogged: false }));
      })
      .then(this.setState(() => ({ ready: true })));
  }
  render() {
    if (!this.state.isLogged && this.state.ready) {
      // return <UserLoginScreen />;
      return <Navigator />;
    } else if (this.state.isLogged && this.state.ready) {
      return <AppContainer />;
    } else return null;
  }
}

export default App;
